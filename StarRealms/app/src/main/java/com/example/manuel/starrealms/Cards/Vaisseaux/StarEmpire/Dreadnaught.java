package com.example.manuel.starrealms.Cards.Vaisseaux.StarEmpire;

import com.example.manuel.starrealms.Cards.Modele.Vaisseau;
import com.example.manuel.starrealms.Cards.Modele.CarteFeraille;
import com.example.manuel.starrealms.Entity.Faction;
import com.example.manuel.starrealms.Entity.Joueur;
import com.example.manuel.starrealms.R;


public class Dreadnaught extends Vaisseau implements CarteFeraille {

    public Dreadnaught(){

        this.nom = "Dreadnaught";
        this.faction = Faction.STAR_EMPIRE;
        this.idImage = R.drawable.dreadnaught;
        this.cout = 7;
        this.abiliteeUtilise = false;
        this.abiliteeAllieUtilise = false;
    }

    @Override
    public void ferailler(Joueur j) {
        j.ajouteDegat(5);
    }

    @Override
    public void jouerCarte(Joueur j) {
        j.ajouteDegat(7);
        j.pigerCarte(1);
    }

    @Override
    public int ajouteCombatFeraille(){
        return 5;
    }
}
