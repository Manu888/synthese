package com.example.manuel.starrealms.Cards.Vaisseaux.StarEmpire;

import com.example.manuel.starrealms.Cards.Modele.Vaisseau;
import com.example.manuel.starrealms.Cards.Modele.CarteFeraille;
import com.example.manuel.starrealms.Entity.Faction;
import com.example.manuel.starrealms.Entity.IA;
import com.example.manuel.starrealms.Entity.Joueur;
import com.example.manuel.starrealms.R;


public class SurveyShip extends Vaisseau implements CarteFeraille {

    public SurveyShip(){

        this.nom = "SurveyShip";
        this.faction = Faction.STAR_EMPIRE;
        this.idImage = R.drawable.surveyship;
        this.cout = 3;
        this.abiliteeUtilise = false;
        this.abiliteeAllieUtilise = false;
    }

    @Override
    public void ferailler(Joueur j) {
        if(j instanceof IA){
            j.faireChoixDefausse((IA)j);
        }
        else{
            j.adversaireDefausse();
        }
    }

    @Override
    public void jouerCarte(Joueur j) {
        j.ajouteOr(1);
        j.pigerCarte(1);

    }
}
