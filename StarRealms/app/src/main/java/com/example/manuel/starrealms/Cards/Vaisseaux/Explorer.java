package com.example.manuel.starrealms.Cards.Vaisseaux;

import com.example.manuel.starrealms.Cards.Modele.Vaisseau;
import com.example.manuel.starrealms.Cards.Modele.CarteFeraille;
import com.example.manuel.starrealms.Entity.Faction;
import com.example.manuel.starrealms.Entity.Joueur;
import com.example.manuel.starrealms.R;


public class Explorer extends Vaisseau implements CarteFeraille {

    public Explorer(){
        this.nom = "Explorer";
        this.faction = Faction.AUCUNE;
        this.idImage = R.drawable.explorer;
        this.cout = 2;
        this.abiliteeUtilise = false;
        this.abiliteeAllieUtilise = false;
    }

    @Override
    public void ferailler(Joueur j) {
        j.ajouteDegat(2);
    }

    @Override
    public void jouerCarte(Joueur j) {
        j.ajouteOr(2);
    }

    @Override
    public int ajouteCombatFeraille(){
        return 2;
    }
}
