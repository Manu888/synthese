package com.example.manuel.starrealms.Entity;




import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.widget.ArrayAdapter;

import com.example.manuel.starrealms.Cards.Bases.Blob.BlobWorld;
import com.example.manuel.starrealms.Cards.Bases.MachineCult.BrainWorld;
import com.example.manuel.starrealms.Cards.Bases.MachineCult.Junkyard;
import com.example.manuel.starrealms.Cards.Bases.MachineCult.MachineBase;
import com.example.manuel.starrealms.Cards.Bases.StarEmpire.FleetHQ;
import com.example.manuel.starrealms.Cards.Bases.StarEmpire.RecyclingStation;
import com.example.manuel.starrealms.Cards.Bases.TradeFederation.BarterWorld;
import com.example.manuel.starrealms.Cards.Bases.TradeFederation.DefenseCenter;
import com.example.manuel.starrealms.Cards.Bases.TradeFederation.PortOfCall;
import com.example.manuel.starrealms.Cards.Bases.TradeFederation.TradingPost;
import com.example.manuel.starrealms.Cards.Modele.AvantPoste;
import com.example.manuel.starrealms.Cards.Modele.Base;
import com.example.manuel.starrealms.Cards.Modele.Carte;
import com.example.manuel.starrealms.Cards.Modele.CarteAlliee;
import com.example.manuel.starrealms.Cards.Modele.Vaisseau;
import com.example.manuel.starrealms.Cards.Vaisseaux.Explorer;
import com.example.manuel.starrealms.Cards.Vaisseaux.MachineCult.MissileBot;
import com.example.manuel.starrealms.Cards.Vaisseaux.MachineCult.PatrolMech;
import com.example.manuel.starrealms.Cards.Vaisseaux.MachineCult.StealthNeedle;
import com.example.manuel.starrealms.Cards.Vaisseaux.MachineCult.SupplyBot;
import com.example.manuel.starrealms.Cards.Vaisseaux.MachineCult.TradeBot;
import com.example.manuel.starrealms.Cards.Vaisseaux.Scout;
import com.example.manuel.starrealms.Cards.Vaisseaux.StarEmpire.ImperialFrigate;
import com.example.manuel.starrealms.Cards.Vaisseaux.StarEmpire.SurveyShip;
import com.example.manuel.starrealms.Cards.Vaisseaux.Viper;
import com.example.manuel.starrealms.Interface.GameActivity;
import com.example.manuel.starrealms.R;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toList;

public class IA extends Joueur {

    private String difficulte;
    protected Comparator<Base> utiliserBasePtsDescendant = (b1, b2) -> Integer.compare(getBasePtsUtilisation(b2), getBasePtsUtilisation(b1));
    protected Comparator<Carte> joueurCartePtsDescendant = (c1, c2) -> Integer.compare(getJouerCartePts(c2), getJouerCartePts(c1));
    protected Comparator<Carte> carteAchetablePtsDescendant = (c1, c2) -> Integer.compare(getAchatCartePts(c2), getAchatCartePts(c1));
    protected Comparator<Carte> feraillerPtsDescendant = (c1, c2) -> Integer.compare(getFerailleCartePts(c2), getFerailleCartePts(c1));
    protected Comparator<Base> attaqueBasePtsDescendant = (c1, c2) -> Integer.compare(getAttaqueBasePts(c2), getAttaqueBasePts(c1));
    protected Comparator<Carte> defaussePtsDescendant = (c1, c2) -> Integer.compare(getDefausseCartePts(c2), getDefausseCartePts(c1));
    protected Comparator<Base> detruireBasePtsDescendant = (c1, c2) -> Integer.compare(getDetruireBasePts(c2), getDetruireBasePts(c1));
    protected Comparator<Carte> getFeraillerPtsDescendant = (c1, c2) -> Integer.compare(getFeraillerCartePts(c2), getFeraillerCartePts(c1));
    protected Comparator<Carte> feraillerDuMarchePtsDescendant = (c1, c2) -> Integer.compare(getFerailleMarchePts(c2), getFerailleMarchePts(c1));
    protected Comparator<Carte> achatCarteSurDeckPtsDescendant = (c1, c2) -> Integer.compare(getCarteSurDeckPts(c2), getCarteSurDeckPts(c1));
    protected Comparator<Carte> copieVaisseauPtsDescendant = (c1, c2) -> Integer.compare(getCopieVaisseauPts((Vaisseau) c2), getCopieVaisseauPts((Vaisseau) c1));
    private AlertDialog.Builder builderSingle;
    private ArrayAdapter<String> arrayAdapter;
    private String carteJouer = "";
    private String carteAcheter = "";
    private String baseDetruite = "";
    private String attaqueDirecte = "";


    public IA(GameActivity act) {
        super(act);
    }

    // ####################################################### GETTERS ET SETTERS ######################################################################################

    public String getCarteJouer() {
        return carteJouer;
    }

    public void setCarteJouer(String carteJouer) {
        this.carteJouer = carteJouer;
    }

    public String getCarteAcheter() {
        return carteAcheter;
    }

    public void setCarteAcheter(String carteAcheter) {
        this.carteAcheter = carteAcheter;
    }

    public String getBaseDetruite() {
        return baseDetruite;
    }

    public void setBaseDetruite(String baseDetruite) {
        this.baseDetruite = baseDetruite;
    }

    public String getAttaqueDirecte() {
        return attaqueDirecte;
    }

    public void setAttaqueDirecte(String attaqueDirecte) {
        this.attaqueDirecte = attaqueDirecte;
    }

    public String getDifficulte() {
        return difficulte;
    }

    public void setDifficulte(String difficulte) {
        this.difficulte = difficulte;
    }

    // Méthode général qui permet à l'IA de réaliser un tour

    public void faireTour(){

        boolean tourFini = false;

        while(!tourFini){

            tourFini=true;

            // On fait le tour des cartes qu'on devrait peut-etre férailler
            List<Carte> carteAFerailler = new ArrayList<>();
            for(Carte c : getEnJeu()){
                if(c.isFeraille()){
                    // Vérifie si on devrait férailler
                    if(devraitFerailler(c)){
                        carteAFerailler.add(c);
                    }
                }
            }

            if(!carteAFerailler.isEmpty()){
                List <Carte> carteClasse = carteAFerailler.stream().sorted(feraillerPtsDescendant).collect(toList());
                for(Carte c : carteClasse){
                    feraillerCarte(c);
                }
            }

            List<Base> basesInutilise = getBaseInutilisee();

            if(!basesInutilise.isEmpty()){
                List <Base> baseClasse = basesInutilise.stream().sorted(utiliserBasePtsDescendant).collect(toList());
                for(Base b : baseClasse){
                    if(!b.isAbiliteeUtilise()){
                        jouerCarte(b);
                        carteJouer += "\n"+ b.getNom();

                    }
                }
            }

            if(!getMain().isEmpty()){
                while(!getMain().isEmpty()){
                    List<Carte> carteClasse = getMain().stream().sorted(joueurCartePtsDescendant).collect(toList());
                    Carte c = carteClasse.get(0);
                    jouerCarte(c);
                    carteJouer += "\n"+c.getNom();
                }
            }

            for(Carte c: getEnJeu()){
                if(c instanceof CarteAlliee){
                    if(!c.isAbiliteeAllieUtilise()){
                        boolean autreCartedeFactionJouer = false;
                        for(Carte c2 : getEnJeu()){
                            if(c.getFaction() == c2.getFaction() && c.getNom() != c2.getNom()){
                                autreCartedeFactionJouer = true;
                            }
                        }
                        if(factionJouerCeTour(c.getFaction()) && autreCartedeFactionJouer){
                            ((CarteAlliee)c).alliee(this);
                            c.setAbiliteeAllieUtilise(true);
                            //tourFini = false;
                        }
                    }
                }
            }

            basesInutilise = getBaseInutilisee();

            if(!basesInutilise.isEmpty()){
                List <Base> baseClasse = basesInutilise.stream().sorted(utiliserBasePtsDescendant).collect(toList());
                for(Base b : baseClasse){
                    if(!b.isUtilisee()){
                        jouerCarte(b);
                        carteJouer += "\n" + b.getNom();
                    }
                }
            }

            carteAFerailler = new ArrayList<>();

            for(Carte c : getEnJeu()){
                if(c.isFeraille()){
                    if(devraitFerailler(c)){
                        carteAFerailler.add(c);
                    }
                }
            }

            if(!carteAFerailler.isEmpty()){
                //tourFini = false;
                List <Carte> carteClasse = carteAFerailler.stream().sorted(feraillerPtsDescendant).collect(toList());
                for(Carte c : carteClasse){
                    feraillerCarte(c);
                }
            }

            if(getOr()>0){
                List<Carte> carteAacheter = getCarteAchetable();
                if(!carteAacheter.isEmpty()){
                    for(Carte c : carteAacheter){
                        acheterCarte(c);
                        carteAcheter += "\n"+ c.getNom()+" au coût de "+ c.getCout() + " or";
                    }
                }
            }
            ajouterBrasse();
        }
        attaquerFinDeTour();
        if(!baseDetruite.equals("")){
            afficherBaseDetruite();
            setBaseDetruite("");
        }
        if(!attaqueDirecte.equals("")){
            afficherAttaqueDirecte();
            setAttaqueDirecte("");
        }
        afficherAchat();
        setCarteAcheter("");
        afficherTour();
        setCarteJouer("");
    }
    // Permet d'obtenir le pointage des bases selon leur type
    public int getBasePtsUtilisation(Base b) {
        if (b instanceof RecyclingStation) {
            return 100;
        } else if (b instanceof MachineBase) {
            return 90;
        } else if (b instanceof Junkyard) {
            return 80;
        } else if (b instanceof BrainWorld) {
            return 70;
        } else if (b instanceof BlobWorld) {
            return 10;
        } else {
            return 20;
        }
    }

    // Permet d'obtenir le pointage de defausse des cartes
    public int getDefausseCartePts(Carte card) {
        if (card instanceof Viper) {
            return 100;
        } else if (card instanceof Scout) {
            return 90;
        } else if (card instanceof Explorer) {
            return 80;
        }

        return 20 - card.getCout();
    }

    // Permet d'obtenir le pointage des cartes à jouer
    public int getJouerCartePts(Carte card) {
        if (card instanceof FleetHQ) {
            return 100;
        } else if (card instanceof RecyclingStation) {
            return 90;
        } else if (card instanceof SupplyBot) {
            return 80;
        } else if (card instanceof TradeBot) {
            return 70;
        } else if (card instanceof MissileBot) {
            return 60;
        } else if (card instanceof PatrolMech) {
            return 50;
        } else if (card instanceof BrainWorld) {
            return 40;
        } else if (card instanceof Explorer) {
            return 10;
        } else if (card instanceof StealthNeedle) {
            return 8;
        } else if (card instanceof Scout) {
            return 6;
        } else if (card instanceof Viper) {
            return 4;
        } else {
            return 20;
        }
    }


    public int getAchatCartePts(Carte c) {
        return c.getCout();
    }

    public int getAttaqueBasePts(Base card) {
        return getDestruireBasePts(card);
    }

    private boolean devraitFerailler(Carte c) {
        return getFerailleCartePts(c) > 0;
    }

    //Permet de savoir qu'elle base l'IA doit détruire en premier
    public int getDestruireBasePts(Base b) {
        int nbCartesBase = getAdversaire().compteCarteParType(getAdversaire().toutesLesCartes(), Carte::isCarteBase);

        if (b instanceof BrainWorld) {
            if (nbCartesBase >= 8) {
                return 12;
            } else if (nbCartesBase >= 6) {
                return 11;
            } else if (nbCartesBase >= 4) {
                return 10;
            }
        } else if (b instanceof MachineBase) {
            if (nbCartesBase >= 8) {
                return 11;
            } else if (nbCartesBase >= 6) {
                return 10;
            } else if (nbCartesBase >= 4) {
                return 9;
            }
        } else if (b instanceof Junkyard) {
            if (nbCartesBase >= 8) {
                return 8;
            } else if (nbCartesBase >= 6) {
                return 7;
            }
        } else if (b instanceof BlobWorld) {
            int opponentBlobCards = getAdversaire().compteCarteParType(getAdversaire().toutesLesCartes(), Carte::isBlob);
            if (opponentBlobCards >= 6) {
                return 10;
            } else if (opponentBlobCards >= 3) {
                return 9;
            }
        } else if (b instanceof RecyclingStation) {
            if (nbCartesBase >= 8) {
                return 6;
            } else if (nbCartesBase >= 5) {
                return 5;
            }
        }

        return b.getCout();
    }


    // Méthode permettant d'obtenir le pointage de féraille d'une carte
    public int getFerailleCartePts(Carte card) {
        int vie = getVie();
        int vieAdversaire = getAdversaire().getVie();
        int nbAvantPosteAdversaire = getAdversaire().getAvantPoste().size();
        int deck = getNbBrasse()+1;

        if (nbAvantPosteAdversaire == 0 && card.ajouteCombatFeraille() >= vieAdversaire) {
            return 10;
        }

        if (card.detruireBaseFeraille()) {
            if (nbAvantPosteAdversaire > 0 && getDegat() >= vieAdversaire) {
                return 10;
            }
        }

        if (card instanceof ImperialFrigate ) {
            if (vieAdversaire <= 10 || vie <= 10) {
                return 5;
            }
        }

        if (card instanceof PortOfCall) {
            if (vie >= 20 && vieAdversaire <= 10) {
                return 5;
            }
        }

        if (card instanceof SurveyShip ) {
            if (getAdversaire().getMain().size() <= 4 || vieAdversaire <= 10 || vie <= 10) {
                return 5;
            }
        }

        if (card.ajouteOrFeraille() > 0) {
            int ptsAchatAugmente = getAchatPtsAugmente(card.ajouteOrFeraille());;
            if ((deck < 3 && ptsAchatAugmente >= 20) || ptsAchatAugmente >= 40) {
                return 5;
            }
        }

        if (getMain().isEmpty() && card.detruireBaseFeraille() && nbAvantPosteAdversaire > 0 && (vieAdversaire <= 10 || vie <= 10)) {
            return 5;
        }

        if (getMain().isEmpty() && card.ajouteCombatFeraille() > 0 && peutDetruireBaseAvecDegat(card.ajouteCombatFeraille())) {
            return 5;
        }

        if (card instanceof Explorer) {
            if (deck > 2) {
                return 5;
            }
        }

        return 0;
    }

    public int getFeraillerCartePts(Carte card) {
        if (card instanceof Viper) {
            return 100;
        } else if (card instanceof Scout) {
            return 90;
        } else if (card instanceof Explorer) {
            return 80;
        }

        return 20 - card.getCout();
    }

    // Fonction qui permet de savoir quel base l'IA  doit détruire en premier
    public int getDetruireBasePts(Base card) {
        int nbCarteBase = getAdversaire().compteCarteParType(getAdversaire().toutesLesCartes(), Carte::isBase);

        if (card instanceof BrainWorld) {
            if (nbCarteBase >= 8) {
                return 12;
            } else if (nbCarteBase >= 6) {
                return 11;
            } else if (nbCarteBase >= 4) {
                return 10;
            }
        } else if (card instanceof MachineBase) {
            if (nbCarteBase >= 8) {
                return 11;
            } else if (nbCarteBase >= 6) {
                return 10;
            } else if (nbCarteBase >= 4) {
                return 9;
            }
        } else if (card instanceof Junkyard) {
            if (nbCarteBase >= 8) {
                return 8;
            } else if (nbCarteBase >= 6) {
                return 7;
            }
        } else if (card instanceof BlobWorld) {
            int opponentBlobCards = getAdversaire().compteCarteParType(getAdversaire().toutesLesCartes(), Carte::isBlob);
            if (opponentBlobCards >= 6) {
                return 10;
            } else if (opponentBlobCards >= 3) {
                return 9;
            }
        } else if (card instanceof RecyclingStation) {
            if (nbCarteBase >= 8) {
                return 6;
            } else if (nbCarteBase >= 5) {
                return 5;
            }
        }

        return card.getCout();
    }


    // Permet de savoir qu'elle carte, l'IA doit férailler du marché
    public int getFerailleMarchePts(Carte card) {
        Faction factionNombreuse = factionLaPlusNombreuse();
        Faction factionNombreuseAdversaire = getAdversaire().factionLaPlusNombreuse();

        if (factionNombreuseAdversaire != null) {
            if (factionNombreuse != null && factionNombreuse == factionNombreuseAdversaire && getOr() >= card.getCout()) {
                return 0;
            }
            if (card.getFaction() == factionNombreuseAdversaire) {
                return card.getCout();
            }
        }

        return 0;
    }

    // Permet de savoir qu'elle carte, le StealthNeedle doit copier
    public int getCopieVaisseauPts(Vaisseau c) {
        if (c instanceof StealthNeedle) {
            return 0;
        }
        return getAchatCartePts(c);
    }

    // Permet de choisir quel carte , l'IA acquiert gratuitement
    public void choisirAchatGratuit(){
        if (!getJeu().getMarche().isEmpty()) {
            Carte c = choisirVaisseauGratuitSurDeck();
            if (c != null) {
                getJeu().getMarche().remove(c);
                getJeu().ajouteNombreCarteAuMarche(1);
                ajouterCarteSurDeck(c);
            }
        }
    }

    // Permet de savoir quel vaisseau on doit mettre sur le deck
    public Carte choisirVaisseauGratuitSurDeck(){
        List<Carte> carteClasse = getJeu().getMarche().stream().filter(Carte::isVaisseau).sorted(achatCarteSurDeckPtsDescendant).collect(toList());

        if (!carteClasse.isEmpty()) {
            Carte cardDessus = carteClasse.get(0);

            if (getCarteSurDeckPts(cardDessus) > 0) {
                return cardDessus;
            }
        }

        return null;
    }

    public int getCarteSurDeckPts(Carte c) {
        return getAchatCartePts(c);
    }

    // Méthode permettant de savoir qu'elle carte, on achète si on ajoute un montant d'or
    public int getAchatPtsAugmente(int extraTrade) {
        int pts = 0;

        List<Carte> cardsToBuy = getCarteAchetable();
        if (!cardsToBuy.isEmpty()) {
            for (Carte cardToBuy : cardsToBuy) {
                pts += getAchatCartePts(cardToBuy);
            }
        }

        List<Carte> sortedCards = getJeu().getMarche().stream().filter(c -> getOr() + extraTrade >= c.getCout()).sorted(carteAchetablePtsDescendant).collect(toList());
        if (!sortedCards.isEmpty()) {
            int ptsMeilleureCarte = getAchatCartePts(sortedCards.get(0));
            return ptsMeilleureCarte - pts;
        }

        return 0;
    }


    // Méthode permettant d'obtenir la liste des cartes achetables pour l'IA
    public List<Carte> getCarteAchetable(){
        List<Carte> cartePotentielle = new ArrayList<>();


        List<Carte> cartesMarche  = getJeu().getMarche();
        cartesMarche.add(getJeu().getExplorer());
        cartesMarche.add(getJeu().getExplorer());

        List<Carte> carteClasse = cartesMarche.stream().filter(c -> getOr() >= c.getCout()).sorted(carteAchetablePtsDescendant).collect(toList());


        if(!carteClasse.isEmpty() && getAchatCartePts(carteClasse.get(0)) > 0){
            Carte cartePlusChere = carteClasse.get(0);

            if(carteClasse.size() >2){
                Map<Carte,Integer> carteAcheterMap = new HashMap<>();

                for(Carte c : cartesMarche){
                    if(!carteAcheterMap.containsKey(c)){
                        carteAcheterMap.put(c,getAchatCartePts(c));
                    }
                }

                List<List<Carte>> listeDeuxCarte = new ArrayList<>();

                for(int j = 1; j< carteClasse.size()-1; j++){
                    Carte carteAcomparer = carteClasse.get(j);
                    for(int o = j+1 ; o < carteClasse.size();o++){
                        if(ajouterDeuxCarteSiPossible(listeDeuxCarte, carteAcomparer,carteClasse.get(o))){
                            break;
                        }
                    }

                }

                for (List<Carte> cardList : listeDeuxCarte) {
                    int totalBuyScore = 0;
                    totalBuyScore += carteAcheterMap.get(cardList.get(0));
                    totalBuyScore += carteAcheterMap.get(cardList.get(1));

                    if (totalBuyScore > carteAcheterMap.get(cartePlusChere)) {
                        cartesMarche.remove(getJeu().getExplorer());
                        cartesMarche.remove(getJeu().getExplorer());
                        return cardList;
                    }
                }
            }
            cartePotentielle.add(cartePlusChere);
        }
        cartesMarche.remove(getJeu().getExplorer());
        cartesMarche.remove(getJeu().getExplorer());
        return  cartePotentielle;

    }

    // Permet de savoir quel carte férailler à partir d'un nombre
    public void feraillerCarteMarche(int nbCarte){
        List<Carte> carteFeraille = choisirCarteFeraillerDuMarche(nbCarte);
        for (Carte c : carteFeraille) {
            getJeu().ferrailleCarteDuMarche(c);
        }

    }
    // Permet de savoir quel carte férailler pour piger à partir d'un nombre
    public void choisirFeraillerPiger(int nbCarte) {
        int nbCarteFeraille = feraillerCarteDeMainOuDefausse(nbCarte);
        if (nbCarteFeraille > 0) {
            pigerCarte(nbCarteFeraille);
        }
    }

    // Permet de faire un choix à savoir si on doit férailler une carte et si c'est optionnelle
    public void choixFerailleMain(boolean optionelle) {
        if (!getMain().isEmpty()) {
            Carte c = getCarteAFeraillerDeMain(optionelle);
            if (c != null) {
                ferailleDeMain(c);
            }
        }
    }
    // Permet d'obtenir la liste des cartes à férailler du marché
    public List<Carte> choisirCarteFeraillerDuMarche(int nbCarte) {
        List<Carte> carteClasse = getJeu().getMarche().stream().sorted(feraillerDuMarchePtsDescendant).collect(toList());

        List<Carte> carteAFerailler = new ArrayList<>();

        for (Carte c : carteClasse) {
            if (carteAFerailler.size() >= nbCarte || getFerailleMarchePts(c) <= 0) {
                break;
            }
            carteAFerailler.add(c);
        }

        return carteAFerailler;
    }
    // Permet de savoir quel carte férailler de sa main
    public Carte getCarteAFeraillerDeMain(boolean optional) {
        if (!getMain().isEmpty()) {
            List<Carte> sortedCards = getMain().stream().sorted(feraillerPtsDescendant).collect(toList());
            Carte c = sortedCards.get(0);
            if (optional && getFeraillerCartePts(c) < 20) {
                return null;
            }
            return c;
        }
        return null;
    }

    public int feraillerCarteDeMainOuDefausse(int nbCarte) {
        List<List<Carte>> cartesAFerailler = getCarteAFeraillerPossible(nbCarte);

        List<Carte> carteFerailleDeDefausse = cartesAFerailler.get(0);
        List<Carte> carteFerailleDeMain = cartesAFerailler.get(1);

        for (Carte c : carteFerailleDeDefausse) {
            ferailleDeDefausse(c);
        }

        for (Carte c : carteFerailleDeMain) {
            ferailleDeMain(c);
        }

        return cartesAFerailler.size();
    }

    public List<List<Carte>> getCarteAFeraillerPossible(int cards) {
        List<List<Carte>> carteFeraille = new ArrayList<>();

        List<Carte> carteFerailleDeDefausse = new ArrayList<>();
        List<Carte> carteFerailleDeMain = new ArrayList<>();

        if (!getDefausse().isEmpty()) {
            List<Carte> sortedDiscardCards = getDefausse().stream().sorted(getFeraillerPtsDescendant).collect(toList());

            for (int i = 0; i < cards; i++) {
                if (sortedDiscardCards.size() <= i) {
                    break;
                }
                Carte card = sortedDiscardCards.get(i);
                int score = getFeraillerCartePts(card);
                if (score < 20) {
                    break;
                } else {
                    carteFerailleDeDefausse.add(card);
                    if(carteFerailleDeDefausse.size() == cards) {
                        break;
                    }
                }
            }
        }

        if (!getMain().isEmpty() && carteFerailleDeDefausse.size() < cards) {
            List<Carte> sortedHandCards = getMain().stream().sorted(getFeraillerPtsDescendant).collect(toList());

            for (int i = 0; i < cards; i++) {
                if (sortedHandCards.size() <= i) {
                    break;
                }
                Carte card = sortedHandCards.get(i);
                int score = getFeraillerCartePts(card);
                if (score < 20) {
                    break;
                } else {
                    carteFerailleDeMain.add(card);
                    if (carteFerailleDeDefausse.size() + carteFerailleDeMain.size() == cards) {
                        break;
                    }
                }
            }
        }

        carteFeraille.add(carteFerailleDeDefausse);
        carteFeraille.add(carteFerailleDeMain);

        return carteFeraille;
    }

    public void defausseEtPige(int cards) {
        int cardsDiscarded = defausserCarte(cards, true);
        pigerCarte(cardsDiscarded);
    }

    public int defausserCarte(int cards, boolean optional) {
        return getCarteADefausser(cards, optional).size();
    }
    public List<Carte> getCarteADefausser(int nbCarte, boolean optional) {
        List<Carte> cardsDefaussable = new ArrayList<>();

        if (!getMain().isEmpty()) {
            if (nbCarte > getMain().size()) {
                nbCarte = getMain().size();
            }
            List<Carte> carteClasse = getMain().stream().sorted(defaussePtsDescendant).collect(toList());
            for (int i = 0; i < nbCarte; i++) {
                Carte card = carteClasse.get(i);
                int pts = getDefausseCartePts(card);
                if (getMain().isEmpty() || (optional && pts < 20)) {
                    break;
                } else {
                    cardsDefaussable.add(card);
                    defausserCarte(card);
                }
            }
        }

        return cardsDefaussable;
    }

    public void copieVaisseau(StealthNeedle stealthNeedle) {
        if (!getEnJeu().isEmpty()) {
            Vaisseau shipToCopy = getShipToCopy();
            if (shipToCopy != null) {
                try {
                    Carte shipToCopyCopy = shipToCopy.getClass().newInstance();
                    shipToCopyCopy.setAbiliteeUtilise(false);
                    stealthNeedle.setCarteCopiee(shipToCopyCopy);
                    this.jouerCarte(stealthNeedle.getCarteCopiee());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
    public Vaisseau getShipToCopy() {
        if (!getEnJeu().isEmpty()) {
            List<Carte> sortedCards = getEnJeu().stream().filter(Carte::isVaisseau).sorted(copieVaisseauPtsDescendant).collect(toList());

            Vaisseau shipACopier = (Vaisseau) sortedCards.get(0);

            if (getCopieVaisseauPts(shipACopier) > 0) {
                return shipACopier;
            }
        }

        return null;
    }
    public void choisirBaseADetruire(){
        if (!getAdversaire().getBase().isEmpty()) {
            Base base = faireChoixBase();
            if (base != null) {
                getAdversaire().detruireBase(base);
            }
        }
    }

    public Base faireChoixBase(){
        if (!getAdversaire().getAvantPoste().isEmpty()) {
            List<Base> sortedOutposts = getAdversaire().getAvantPoste().stream().sorted(detruireBasePtsDescendant).collect(toList());
            Base baseToDestroy = sortedOutposts.get(0);
            if (getDetruireBasePts(baseToDestroy) > 0) {
                return baseToDestroy;
            }
        } else if (!getAdversaire().getBase().isEmpty()) {
            List<Base> sortedBases = getAdversaire().getBase().stream().sorted(detruireBasePtsDescendant).collect(toList());
            Base baseToDestroy = sortedBases.get(0);
            if (getDetruireBasePts(baseToDestroy) > 0) {
                return baseToDestroy;
            }
        }

        return null;
    }

    public void faireChoix(Carte c, Choix... choix) {
        c.choixFait(getChoix(c), this);
    }
    public int getChoix(Carte card) {
        int deck = getNbBrasse()+1;
        int vieAdversaire = getAdversaire().getVie();

        if (card instanceof BarterWorld) {
            if (deck < 3) {
                return 2;
            }
        } else if (card instanceof BlobWorld) {
            int blobCardsPlayed = compteCarteParType(getEnJeu(), Carte::isBlob);
            if (blobCardsPlayed >= 2 && vieAdversaire > 5) {
                return 2;
            }
        } else if (card instanceof DefenseCenter) {
            if (this.peutDetruireBaseAvecDegat(2)) {
                return 2;
            }
            if (vieAdversaire <= 2) {
                return 2;
            }
            if (vieAdversaire < 10 && getVie() > 10) {
                return 2;
            }
        } else if (card instanceof PatrolMech) {
            if (this.peutDetruireBaseAvecDegat(2)) {
                return 2;
            }
            if (deck > 2) {
                return 2;
            }
        } else if (card instanceof RecyclingStation) {
            int buyScoreIncrease = getAchatPtsAugmente(1);
            if (deck <= 3 && buyScoreIncrease >= 20) {
                return 1;
            } else {
                return 2;
            }
        } else if (card instanceof TradingPost) {
            if (deck <= 2) {
                return 1;
            }
            int buyScoreIncrease = getAchatPtsAugmente(1);
            if (getVie() >= 20 && buyScoreIncrease >= 20) {
                return 1;
            } else {
                return 2;
            }
        }

        return 1;
    }
    private boolean ajouterDeuxCarteSiPossible(List<List<Carte>> twoCardList, Carte c1, Carte c2) {
        if ((c1.getCout() + c2.getCout()) <= getOr()) {
            List<Carte> cards = new ArrayList<>(2);
            cards.add(c1);
            cards.add(c2);
            twoCardList.add(cards);
            return true;
        }
        return false;
    }

    public void attaquerFinDeTour(){
        // On attaque les avant-postes en premier car il protège le joueur
        if(getDegat()>0 && !getAdversaire().getAvantPoste().isEmpty()){
            List<AvantPoste> avantPostesClasse = getAdversaire().getAvantPoste().stream().sorted(attaqueBasePtsDescendant).collect(toList());
            for(AvantPoste ap : avantPostesClasse){
                if(getAttaqueBasePts(ap) > 0 && getDegat() >= ap.getVie()){
                    detruireBaseEnnemie(ap);
                    baseDetruite += "\n"+ap.getNom() + " qui avait "+ap.getVie()+" vie(s)";
                }
            }
        }
        else if(getDegat() >= getAdversaire().getVie()){
            // Si on peut tuer l'adversaire avec les degats qu'on a, on l'attaque directement
            // alors que l'adversaire n'a pas d'avant-poste
            attaquerAdversaire();
        }

        if(getDegat() > 0 && ! getAdversaire().getBase().isEmpty()){
            List<Base> baseClasse = getAdversaire().getBase().stream().sorted(attaqueBasePtsDescendant).collect(toList());
            for(Base b : baseClasse){
                if(getAttaqueBasePts(b) > 0 && getDegat() >= b.getVie()){
                    detruireBaseEnnemie(b);
                    baseDetruite += "\n"+b.getNom() + " qui avait "+b.getVie()+" vie(s)";
                }
            }
        }

        if(getDegat() > 0 && getAdversaire().getAvantPoste().isEmpty()){
            attaqueDirecte += "Vous avez été attaquer de "+getDegat();
            attaquerAdversaire();
        }

        finirTour();
    }
    public void afficherTour(){
            this.builderSingle = new AlertDialog.Builder(getActivity());
            this.builderSingle.setIcon(R.drawable.icon);
            this.builderSingle.setTitle("L'IA a jouer ce tour : ");
            this.builderSingle.setMessage(carteJouer);
            this.builderSingle.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog,int which) {
                        dialog.dismiss();
                    }
            });

            this.builderSingle.show();
    }

    public void afficherAchat(){

        this.builderSingle = new AlertDialog.Builder(getActivity());
        this.builderSingle.setIcon(R.drawable.icon);
        this.builderSingle.setTitle("L'IA à acheter ce tour-ci : ");
        this.builderSingle.setMessage(carteAcheter);
        this.builderSingle.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog,int which) {
                dialog.dismiss();
            }
        });

        this.builderSingle.show();

    }

    public void afficherBaseDetruite(){

        this.builderSingle = new AlertDialog.Builder(getActivity());
        this.builderSingle.setIcon(R.drawable.icon);
        this.builderSingle.setTitle("L'IA a détruit les bases suivantes : ");
        this.builderSingle.setMessage(baseDetruite);
        this.builderSingle.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog,int which) {
                dialog.dismiss();
            }
        });

        this.builderSingle.show();

    }

    public void afficherAttaqueDirecte(){
        this.builderSingle = new AlertDialog.Builder(getActivity());
        this.builderSingle.setIcon(R.drawable.icon);
        this.builderSingle.setTitle("Vous avez reçu une attaque directe!");
        this.builderSingle.setMessage(attaqueDirecte);
        this.builderSingle.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog,int which) {
                dialog.dismiss();
            }
        });

        this.builderSingle.show();
    }


}
