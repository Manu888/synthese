package com.example.manuel.starrealms.Cards.Bases.MachineCult;

import com.example.manuel.starrealms.Cards.Modele.AvantPoste;
import com.example.manuel.starrealms.Entity.Faction;
import com.example.manuel.starrealms.Entity.IA;
import com.example.manuel.starrealms.Entity.Joueur;
import com.example.manuel.starrealms.R;


public class Junkyard extends AvantPoste {

    public Junkyard(){

        this.nom = "Junkyard";
        this.faction = Faction.MACHINE_CULT;
        this.idImage = R.drawable.junkyard;
        this.cout = 6;
        this.vie=5;
        this.abiliteeUtilise = false;
    }

    @Override
    public void jouerCarte(Joueur j) {
        if(j instanceof IA){
            ((IA)j).feraillerCarteDeMainOuDefausse(1);
        }
        else{
            j.faireChoixFerraillerMainDefausse();
        }
    }
}
