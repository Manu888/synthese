package com.example.manuel.starrealms.Entity;


// Énumération des factions possibles
public enum Faction {
    AUCUNE,
    BLOB,
    TRADE_FEDERATION,
    MACHINE_CULT,
    STAR_EMPIRE
}
