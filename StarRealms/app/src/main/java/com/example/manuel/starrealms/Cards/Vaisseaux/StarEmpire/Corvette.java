package com.example.manuel.starrealms.Cards.Vaisseaux.StarEmpire;

import com.example.manuel.starrealms.Cards.Modele.Vaisseau;
import com.example.manuel.starrealms.Cards.Modele.CarteAlliee;
import com.example.manuel.starrealms.Entity.Faction;
import com.example.manuel.starrealms.Entity.Joueur;
import com.example.manuel.starrealms.R;


public class Corvette extends Vaisseau implements CarteAlliee {

    public Corvette(){

        this.nom = "Corvette";
        this.faction = Faction.STAR_EMPIRE;
        this.idImage = R.drawable.corvette;
        this.cout = 2;
        this.abiliteeUtilise = false;
        this.abiliteeAllieUtilise = false;
    }

    @Override
    public void alliee(Joueur j) {
            j.ajouteDegat(2);
    }

    @Override
    public void jouerCarte(Joueur j) {
            j.ajouteDegat(1);
            j.pigerCarte(1);
    }
}
