package com.example.manuel.starrealms.Cards.Bases.StarEmpire;

import com.example.manuel.starrealms.Cards.Modele.AvantPoste;
import com.example.manuel.starrealms.Cards.Modele.CarteAlliee;
import com.example.manuel.starrealms.Entity.Faction;
import com.example.manuel.starrealms.Entity.Joueur;
import com.example.manuel.starrealms.R;


public class WarWorld extends AvantPoste implements CarteAlliee {

    public WarWorld(){

        this.nom = "WarWorld";
        this.faction = Faction.STAR_EMPIRE;
        this.idImage = R.drawable.warworld;
        this.cout = 5;
        this.vie = 4;
        this.abiliteeUtilise = false;
        this.abiliteeAllieUtilise = false;
    }

    @Override
    public void alliee(Joueur j) {
        j.ajouteDegat(4);
    }

    @Override
    public void jouerCarte(Joueur j) {
        j.ajouteDegat(3);
    }
}
