package com.example.manuel.starrealms.Cards.Bases.MachineCult;

import com.example.manuel.starrealms.Cards.Modele.AvantPoste;
import com.example.manuel.starrealms.Entity.Faction;
import com.example.manuel.starrealms.Entity.IA;
import com.example.manuel.starrealms.Entity.Joueur;
import com.example.manuel.starrealms.R;


public class MachineBase extends AvantPoste {

    public MachineBase(){

        this.nom = "MachineBase";
        this.faction = Faction.MACHINE_CULT;
        this.idImage = R.drawable.machinebase;
        this.cout = 7;
        this.vie= 6;
        this.abiliteeUtilise = false;
    }

    @Override
    public void jouerCarte(Joueur j) {
        if(j instanceof IA){
            j.pigerCarte(1);
            ((IA)j).choixFerailleMain(false);
        }
        else {
            j.pigerCarte(1);
            j.faireChoixFerraillerMain();
        }
    }
}
