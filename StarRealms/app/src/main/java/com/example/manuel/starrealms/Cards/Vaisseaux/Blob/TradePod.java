package com.example.manuel.starrealms.Cards.Vaisseaux.Blob;

import com.example.manuel.starrealms.Cards.Modele.Vaisseau;
import com.example.manuel.starrealms.Cards.Modele.CarteAlliee;
import com.example.manuel.starrealms.Entity.Faction;
import com.example.manuel.starrealms.Entity.Joueur;
import com.example.manuel.starrealms.R;


public class TradePod extends Vaisseau implements CarteAlliee {

    public TradePod(){

        this.nom = "TradePod";
        this.faction = Faction.BLOB;
        this.idImage = R.drawable.tradepod;
        this.cout = 2;
        this.abiliteeUtilise = false;
        this.abiliteeAllieUtilise = false;
    }

    @Override
    public void alliee(Joueur j) {
            j.ajouteDegat(2);
    }

    @Override
    public void jouerCarte(Joueur j) {
            j.ajouteOr(3);
    }
}
