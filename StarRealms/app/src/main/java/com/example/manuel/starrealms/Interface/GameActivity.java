package com.example.manuel.starrealms.Interface;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.manuel.starrealms.Cards.Modele.AvantPoste;
import com.example.manuel.starrealms.Cards.Modele.Base;
import com.example.manuel.starrealms.Cards.Modele.Carte;
import com.example.manuel.starrealms.Cards.Modele.CarteAlliee;
import com.example.manuel.starrealms.Cards.Vaisseaux.Explorer;
import com.example.manuel.starrealms.Entity.Faction;
import com.example.manuel.starrealms.Entity.IA;
import com.example.manuel.starrealms.Entity.Jeu;
import com.example.manuel.starrealms.Entity.Joueur;
import com.example.manuel.starrealms.R;

import java.util.ArrayList;
import java.util.List;


public class GameActivity extends AppCompatActivity{

    String nom, difficulte;
    Jeu jeu;
    public static Joueur joueur;
    IA ia;
    EcouteurClick ec;
    EcouteurBouton eb;
    Carte carteSelect;
    Explorer exp;

    public static final int REQUEST_CODE_FERAILLE = 100;

    TextView affichageTour, vieJoueur, orJoueur, degatJoueur, vieIA, orIA, degatIA, carteSelectionne, deck, defausse, deckIA, defausseIA, carteEnJeu;
    ImageView imageExplorer;
    Button boutonAttaquer, boutonJouer, boutonAcheter,boutonFinir;
    LinearLayout layoutMain, layoutMarche, layoutBaseEnJeu,layoutBaseEnJeuIA, layoutInfoIA, layoutInfoJoueur;

    AlertDialog.Builder builderSingle;
    ArrayAdapter<String> arrayAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        // On crée des écouteurs
        ec = new EcouteurClick();
        eb = new EcouteurBouton();

        // On met le message de choix d'attaque de la boite de dialog et l'icône
        builderSingle = new AlertDialog.Builder(GameActivity.this);
        builderSingle.setIcon(R.drawable.icon);
        builderSingle.setTitle("Choissisez la cible de votre attaque : ");

        // On crée un bouton anuller qui ferme la boite de dialog
        builderSingle.setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        // On récupère toutes les éléments de la vue
        affichageTour = (TextView)findViewById(R.id.numTour);
        vieJoueur = (TextView)findViewById(R.id.vieJoueur);
        orJoueur = (TextView)findViewById(R.id.orJoueur);
        degatJoueur = (TextView)findViewById(R.id.degatJoueur);
        vieIA = (TextView)findViewById(R.id.vieIA);
        orIA = (TextView)findViewById(R.id.orIA);
        degatIA = (TextView)findViewById(R.id.degatIA);
        carteSelectionne = (TextView) findViewById(R.id.carteSelect);
        deck = (TextView)findViewById(R.id.deck);
        defausse = (TextView)findViewById(R.id.defausse);
        deckIA = (TextView)findViewById(R.id.deckIA);
        defausseIA = (TextView)findViewById(R.id.defausseIA);
        carteEnJeu = (TextView)findViewById(R.id.enJeu);

        imageExplorer = (ImageView)findViewById(R.id.explorer);
        // On crée un explorer et on l'associe à l'image d'explorer
        exp = new Explorer();
        imageExplorer.setTag(exp);

        boutonAttaquer = (Button)findViewById(R.id.boutonAttaquer);
        boutonJouer = (Button)findViewById(R.id.boutonJouer);
        boutonAcheter = (Button)findViewById(R.id.boutonAcheter);
        boutonFinir = (Button)findViewById(R.id.boutonFinir);

        boutonAttaquer.setOnClickListener(eb);
        boutonJouer.setOnClickListener(eb);
        boutonAcheter.setOnClickListener(eb);
        boutonFinir.setOnClickListener(eb);

        layoutBaseEnJeu = (LinearLayout)findViewById(R.id.LayoutenJeu);
        layoutMain = (LinearLayout)findViewById(R.id.LayoutMain);
        layoutInfoIA = (LinearLayout)findViewById(R.id.LayoutInfoIA);
        layoutInfoJoueur = (LinearLayout)findViewById(R.id.LayoutInfoJoueur);
        layoutMarche = (LinearLayout)findViewById(R.id.LayoutMarche);
        layoutBaseEnJeuIA = (LinearLayout)findViewById(R.id.LayoutenJeuIA);

        // Pour chacune des cartes dans le marché, on associe un écouteur
        int childCount = layoutMarche.getChildCount();
        for (int i = 0; i < childCount; i++) {
            ImageView v = (ImageView)layoutMarche.getChildAt(i);
            v.setOnClickListener(ec);

        }

        // Pour chacune des bases en jeu, on associe un écouteur
        childCount = layoutBaseEnJeu.getChildCount();
        for(int i =0;i<childCount;i++){
            ImageView v = (ImageView)layoutBaseEnJeu.getChildAt(i);
            v.setOnClickListener(ec);
        }

        // Pour chacune des bases de l'IA, on associe un écouteur
        childCount = layoutBaseEnJeuIA.getChildCount();
        for(int i =0;i<childCount;i++){
            ImageView v = (ImageView)layoutBaseEnJeuIA.getChildAt(i);
            v.setOnClickListener(ec);
        }

        // Pour chacune des cartes dans la main, on associe un écouteur
        childCount = layoutMain.getChildCount();
        for(int i =0;i<childCount;i++){
            ImageView v = (ImageView)layoutMain.getChildAt(i);
            v.setOnClickListener(ec);
        }

        // On met un écouteur sur chacune des cartes qu'on peut cliquer
        carteSelectionne.setOnClickListener(ec);
        defausse.setOnClickListener(ec);
        defausseIA.setOnClickListener(ec);
        carteEnJeu.setOnClickListener(ec);

        // On récupère le nom du joueur et la difficulté choisie
        Intent myIntent = getIntent();
        nom = myIntent.getStringExtra("nom");
        difficulte = myIntent.getStringExtra("difficulte");

        // On crée un objet de jeu et on initialise la partie
        jeu = new Jeu();
        jeu.initialiserPartie(this);

        // On récupère le joueur et on met son nom
        joueur = jeu.getJoueur();
        joueur.setNom(nom);

        // On récupère l'IA, on met son nom et sa difficulté
        ia = jeu.getIa();
        ia.setDifficulte(difficulte);
        ia.setNom("IA");

        // Si le joueur ne débute pas, l'IA prend son premier tour
        if(!joueur.isDebute()){
            ia.faireTour();
            raffraichirAffichage();
            jeu.tourFini();
        }
        // On raffraichit l'affichage à la fin pour afficher les changements
        raffraichirAffichage();

    }

    // CETTE FONCTION EST APPELER APRÈS UN STARTACTIVITYFORRESULT()
    // Permet de savoir quel carte le joueur désire ferailler afin d'activer l'abilité associée
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == REQUEST_CODE_FERAILLE) {
            // Si on a un choix qui a bien été fait dans la liste des brasseries
            if (resultCode == EnJeuActivity.RESULT_OK){
                // on récupère la carte et on la féraille
                Carte c = (Carte)data.getSerializableExtra("carte");
                joueur.feraillerCarte(c);
                raffraichirAffichage();
                }
            }
        }

    public class EcouteurClick implements View.OnClickListener{

        @Override
        public void onClick(View view) {
            if(view == carteSelectionne){
                // Si le joueur appuie sur la carte sélectionnée, on démarre
                // l'activité d'information détaillée
                Intent i = new Intent(GameActivity.this,InfoDetailleActivity.class);
                i.putExtra("idImage",carteSelect.getIdImage());
                boolean rotate = carteSelect.isBase();
                i.putExtra("rotation",rotate);

                startActivity(i);
            }
            else if(view == defausse){
                // Si le joueur n'a pas de defausse, on lui affiche un message
                if(joueur.getDefausse().size() == 0){
                    Context context = getApplicationContext();
                    CharSequence text ="Il n'y a aucune carte dans la défausse";
                    int duree = Toast.LENGTH_SHORT;
                    Toast toast = Toast.makeText(context, text, duree);
                    toast.show();
                }
                else{
                    // Sinon on récupère toutes les codes d'images des cartes de la defausse
                    Intent i = new Intent(GameActivity.this,DefausseActivity.class);
                    ArrayList<Integer> listesID = new ArrayList<Integer>();

                    for(Carte c : joueur.getDefausse()){
                        listesID.add(c.getIdImage());
                    }

                    // On ajoute les codes à l'intention et on démarre l'activité
                    i.putIntegerArrayListExtra("defausse",listesID);
                    startActivity(i);
                    }

            }
            else if(view == defausseIA){
                // On affiche un message si l'IA n'a pas de défausse
                if(ia.getDefausse().size() == 0){
                    Context context = getApplicationContext();
                    CharSequence text ="Il n'y a aucune carte dans la défausse de l'IA";
                    int duree = Toast.LENGTH_SHORT;
                    Toast toast = Toast.makeText(context, text, duree);
                    toast.show();
                }
                else{
                    // Sinon on récupère toutes les codes des images de la defausse de l'IA
                    Intent i = new Intent(GameActivity.this,DefausseActivity.class);
                    ArrayList<Integer> listesID = new ArrayList<Integer>();

                    for(Carte c : ia.getDefausse()){
                        listesID.add(c.getIdImage());

                    }
                    // On ajoute les codes à l'intention et on démarre l'activité
                    i.putIntegerArrayListExtra("defausse",listesID);
                    startActivity(i);
                }
            }
            else if(view == carteEnJeu){
                // Si on a pas de carte jouée, on affiche un message
                if(joueur.getEnJeu().size() == 0){
                    Context context = getApplicationContext();
                    CharSequence text ="Il n'y a aucune carte jouée ce tour";
                    int duree = Toast.LENGTH_SHORT;
                    Toast toast = Toast.makeText(context, text, duree);
                    toast.show();
                }
                else{
                    // Sinon on met toutes les cartes dans l'intention et on démarre l'activité
                    // avec un intention de retour potentielle si le joueur désire férailler une carte
                    Intent i = new Intent(GameActivity.this,EnJeuActivity.class);
                    for(int o = 0 ; o< joueur.getEnJeu().size();o++){
                        i.putExtra(Integer.toString(o),joueur.getEnJeu().get(o));
                    }
                    startActivityForResult(i,REQUEST_CODE_FERAILLE);
                }
            }
            else{
                //Si on a pas cliquer sur une zone qui démarre une autre activité,
                // on associe l'image et la carte à la zone de carte sélectionnée
                ImageView image = ((ImageView) view);
                Carte c = (Carte)image.getTag();
                int code = getDrawableId(image);
                carteSelectionne.setBackgroundResource(code);
                carteSelect = c;
            }
        }
    }

    public class EcouteurBouton implements View.OnClickListener{

        @Override
        public void onClick(View view) {

            if(view == boutonAcheter){

                // On doit d'abord vérifier que la carte selectionnée est valide
                // c'est a dire qu'elle est dans le marché ou un explorer
                boolean achatValid = false;
                List<Carte> marche = jeu.getMarche();
                for(int i =0; i<marche.size();i++){
                    if(marche.get(i) == carteSelect){
                        achatValid=true;
                    }
                }
                if(carteSelect == exp){
                    achatValid=true;
                }

                // Si l'achat est valide, on vérifie le cout de la carte
                if(achatValid){
                    if(joueur.getOr() >= carteSelect.getCout()){
                        // Si c'est un explorer un reinitialise un nouveau explorer
                        if(carteSelect == exp){
                            // ON REINITIALISE L'EXPLORER
                            exp = new Explorer();
                            imageExplorer.setTag(exp);
                        }
                        // Le joueur achete la carte et il n'y a plus de carte sélectionnée
                        joueur.acheterCarte(carteSelect);
                        carteSelectionne.setBackgroundResource(android.R.color.transparent);
                        carteSelect = null;
                    }
                    else // Le joueur n'a pas assez d'or pour acheter la carte
                        Toast.makeText(GameActivity.this, "Vous n'avez pas assez d'or pour acheter cette carte",
                                Toast.LENGTH_LONG).show();
                }
                else{// Sinon on affiche un message d'erreur
                    Toast.makeText(GameActivity.this, "La carte que vous tentez d'acheter n'est pas dans le marché",
                            Toast.LENGTH_LONG).show();
                }
                raffraichirAffichage();
            }else if(view == boutonFinir){
                // Si le joueur a fini son tour, c'est maintenant le tour de l'IA
                joueur.finirTour();
                jeu.tourFini();
                ia.faireTour();
                raffraichirAffichage();
                jeu.tourFini();
                checkEndGame();

            }
            else if(view == boutonAttaquer){
                // On vérifie que le joueur a des dégats à distribuer
                if(joueur.getDegat()>0){
                    arrayAdapter= new ArrayAdapter<String>(GameActivity.this, android.R.layout.select_dialog_singlechoice);
                    builderSingle.setTitle("Vous avez "+ Integer.toString(joueur.getDegat())+" dégats à attribuer \n Choissisez la cible : ");
                    List<Base> bases = ia.getBase();
                    List<AvantPoste> postes = ia.getAvantPoste();
                    // On ajoute la liste des avantpostes que le joueur doit détruire en premier
                    // ou bien on ajoute la liste des bases simples qu'il peut attaquer
                    if(ia.getAvantPoste().size()== 0){
                        arrayAdapter.add(ia.getNom());
                        for(Base b : bases){
                            arrayAdapter.add("Nom : " + b.getNom() +" ; Vie : " + Integer.toString(b.getVie()));
                        }
                    }
                    else{
                        // S
                        for(AvantPoste ap : postes){
                            arrayAdapter.add("Nom : " + ap.getNom() +" ; Vie : " + Integer.toString(ap.getVie()));
                        }
                    }

                    builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // L'écouteur à l'intérieur de la liste permet de récuperer la cible de l'ataquer
                            String nomCible = arrayAdapter.getItem(which);
                            AlertDialog.Builder builderInner = new AlertDialog.Builder(GameActivity.this);
                            builderInner.setMessage(nomCible);
                            // Si le joueur a choisi l'IA, on inflige les dégats directement
                            if(nomCible.equals(ia.getNom())){
                                joueur.attaquerAdversaire();
                                checkEndGame();
                            }
                            else{
                                // On cherche parmi les bases la cible de notre attaque puis si on a pas attaquer, on cherche parmi les avant-postes
                                boolean attaque = false;
                                if(!attaque){
                                    for(Base b : bases){
                                        if(nomCible.toLowerCase().contains(b.getNom().toLowerCase())){
                                            // On vérifie qu'on a assez de dégat pour attaque la base puis on lui inflige
                                            if(b.getVie()<= joueur.getDegat()){
                                                // On détruit la base qu'on a attaquer
                                                joueur.detruireBaseEnnemie(b);
                                                attaque = true;
                                                break;
                                            }
                                            else{
                                                // Sinon on affiche un message qui nous informe du manque de dégats
                                                Toast.makeText(GameActivity.this, "Vous n'avez pas assez de dégat pour attaquer cette base",
                                                        Toast.LENGTH_LONG).show();
                                            }
                                        }
                                    }
                                }
                                if(!attaque) {
                                    // Si on a toujours pas attaquer, on cherche parmi les avants-postes
                                    for (AvantPoste ap : postes) {
                                        if (nomCible.toLowerCase().contains(ap.getNom().toLowerCase())) {
                                            if (ap.getVie() <= joueur.getDegat()) {
                                                // On détruit la base qu'on a attaquer
                                                joueur.detruireBaseEnnemie(ap);
                                                break;
                                            }
                                        } else {
                                            Toast.makeText(GameActivity.this, "Vous n'avez pas assez de dégat pour attaquer cet avant-poste",
                                                    Toast.LENGTH_LONG).show();
                                        }
                                    }
                                }
                            }
                            raffraichirAffichage();
                            // On affiche une boite de notification d'attaque montrant la cible de l'attaque
                            builderInner.setTitle("Vous avez attaquer la cible suivante :");
                            builderInner.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog,int which) {
                                    dialog.dismiss();
                                }
                            });
                            builderInner.show();
                        }
                    });
                    builderSingle.show();
                }
                else{ // Sinon on affiche un message
                    Toast.makeText(GameActivity.this, "Vous n'avez pas de dégat à infliger",
                            Toast.LENGTH_LONG).show();
                }
                raffraichirAffichage();
            }
            else if(view == boutonJouer){

                // On doit vérifier si la carte sélectionnée est valide
                boolean jouerValid = false;

                // On vérifie si la carte est dans la main du joueur
                List<Carte> main = joueur.getMain();
                for(int i =0; i<main.size();i++){
                    if(main.get(i) == carteSelect){
                        jouerValid=true;
                    }
                }
                // On vérifie si le joueur veut jouer/activer une base deja en jeu
                if(!jouerValid){
                    List<Base> bases = joueur.getBase();
                    for(int i =0; i<bases.size();i++){
                        if(bases.get(i) == carteSelect){
                            jouerValid=true;
                        }
                    }
                }
                if(jouerValid){
                    // Si l'abilitée de la carte sélectionnée n'est pas utilisée, on peut l'utilisé
                    if(!carteSelect.isAbiliteeUtilise()) {
                        if(carteSelect instanceof CarteAlliee){
                            // Si c'est une carte alliée, on vérifie si on a pas jouer ue autre carte de cette faction ce tour
                            if(!carteSelect.isAbiliteeAllieUtilise()){
                                if(joueur.factionJouerCeTour(carteSelect.getFaction())){
                                    // SI ON A JOUER UNE CARTE DE CETTE FACTION CE TOUR-CI, ON ACTIVE SON ABILETÉ ALLIÉE
                                    ((CarteAlliee)carteSelect).alliee(joueur);
                                    carteSelect.setAbiliteeAllieUtilise(true);
                                }
                            }
                        }
                        // on joue la carte et on met le marqueur  a true
                        joueur.jouerCarte(carteSelect);
                        carteSelect.setAbiliteeUtilise(true);
                        if(carteSelect.isBase()){
                            ((Base)carteSelect).setUtilisee(true);
                        }

                        // ON DOIT ENSUITE VÉRIFIER L'ACTIVATION EN CASCADE DES ABILETÉS ALLIÉES DES AUTRES CARTES
                        // ON VÉRIFIE LES CARTES QUI ONT ÉTÉ JOUER PRÉCÉDEMENT DANS LE TOUR
                        for(Carte c : joueur.getEnJeu()){
                            if(carteSelect != c && carteSelect != null){
                                if(c instanceof CarteAlliee){
                                    if(c.getFaction() == carteSelect.getFaction()){
                                    if(!c.isAbiliteeAllieUtilise()){
                                        if(joueur.factionJouerCeTour(c.getFaction())){
                                            ((CarteAlliee)c).alliee(joueur);
                                            c.setAbiliteeAllieUtilise(true);
                                        }
                                    }
                                    }
                                }
                            }

                        }
                        // ON VÉRIFIE LES BASES QUI SONT EN JEU
                        for(Base b : joueur.getBase()){
                            if(carteSelect != b && carteSelect != null){
                                if(b instanceof CarteAlliee){
                                    if(!b.isAbiliteeAllieUtilise()){
                                        if(joueur.factionJouerCeTour(b.getFaction())){
                                            ((CarteAlliee)b).alliee(joueur);
                                            b.setAbiliteeAllieUtilise(true);
                                        }
                                    }
                                }
                            }
                        }
                        // On annule la carte sélectionnée et on met l'affiche transparent
                        carteSelectionne.setBackgroundResource(android.R.color.transparent);
                        carteSelect = null;
                    }
                    else// Sinon on affiche un message d'erreur
                        Toast.makeText(GameActivity.this, "Cette carte a déja été utilisée ce tour-ci",
                                Toast.LENGTH_LONG).show();
                }
                else{// On affiche que la carte voulue n'est pas valide
                    Toast.makeText(GameActivity.this, "La carte que vous tentez de jouer n'est pas valide",
                            Toast.LENGTH_LONG).show();
                }
                raffraichirAffichage();

            }

        }
    }

    public void raffraichirAffichage(){
        //

        // ON RAFFRAICHIT L'AFFICHAGE DES INFOS DES JOUEURS ET DE L'IA
        affichageTour.setText("Tour #"+joueur.getTour());
        vieJoueur.setText(Integer.toString(joueur.getVie()));
        orJoueur.setText(Integer.toString(joueur.getOr()));
        degatJoueur.setText(Integer.toString(joueur.getDegat()));
        vieIA.setText(Integer.toString(ia.getVie()));
        orIA.setText(Integer.toString(ia.getOr()));
        degatIA.setText(Integer.toString(ia.getDegat()));
        deck.setText(Integer.toString(joueur.getDeck().size()));
        deckIA.setText(Integer.toString(ia.getDeck().size()));
        defausse.setText(Integer.toString(joueur.getDefausse().size()));
        defausseIA.setText(Integer.toString(ia.getDefausse().size()));

        // ON AFFICHE LA DERNIERE CARTE DU PAQUET DE DÉFAUSSE SINON ON AFFICHE RIEN
        if(joueur.getDefausse().size()>0){
            int dernierIndex = joueur.getDefausse().size()-1;
            // On affiche la derniere carte defausser comme étant celle sur le dessus
            defausse.setBackgroundResource(joueur.getDefausse().get(dernierIndex).getIdImage());
        }else{
            // Si y'en a pas, on affiche de la transparence
            defausse.setBackgroundResource(android.R.color.transparent);
        }

        // Même principe pour la défausse de l'IA
        if(ia.getDefausse().size()>0){
            int dernierIndex = ia.getDefausse().size()-1;
            defausseIA.setBackgroundResource(ia.getDefausse().get(dernierIndex).getIdImage());
        }else{
            defausseIA.setBackgroundResource(android.R.color.transparent);
        }

        // on affiche la derniere carte jouée comme étant celle sur le dessus
        if(joueur.getEnJeu().size()>0){
            int dernierIndex = joueur.getEnJeu().size()-1;
            carteEnJeu.setBackgroundResource(joueur.getEnJeu().get(dernierIndex).getIdImage());
        }else{
            carteEnJeu.setBackgroundResource(android.R.color.transparent);
        }

        // ON RAFFRAICHIT L'IMAGE DES CARTES DU MARCHE A PARTIR DE CE QUE LE JEU DANS SES CARTES DU MARCHE
        List<Carte> marche = jeu.getMarche();
        for(int i = 0; i< marche.size();i++){
            ImageView image = (ImageView)layoutMarche.getChildAt(i);
            image.setTag(marche.get(i));
            image.setImageResource((marche.get(i).getIdImage()));
        }
        // ON RAFFRAICHIT L'IMAGE DES CARTES EN JEU A PARTIR DE CE QUE LE JOUEUR A DANS SES CARTES EN JEU
        // ON EFFACE TOUT LES IMAGES EN JEU
        int childCount = layoutBaseEnJeu.getChildCount();
        for(int i =0;i<childCount;i++){
            ImageView v = (ImageView)layoutBaseEnJeu.getChildAt(i);
            v.setImageResource(android.R.color.transparent);
        }
        // PUIS ON REMET LES IMAGES CORRESPONDANTS A CE QUE LE JOUEUR A EN JEU
        List<Base> enJeu = joueur.getBase();
        layoutBaseEnJeu.setWeightSum(enJeu.size());
        for(int i =0; i<enJeu.size();i++){
            ImageView image = (ImageView)layoutBaseEnJeu.getChildAt(i);
            image.setTag(enJeu.get(i));
            image.setImageResource((enJeu.get(i).getIdImage()));
        }

        // ON FAIT PAREILLE POUR L'IA
        childCount = layoutBaseEnJeuIA.getChildCount();
        for(int i=0; i<childCount;i++){
            ImageView v = (ImageView)layoutBaseEnJeuIA.getChildAt(i);
            v.setImageResource(android.R.color.transparent);
        }
        List <Base> enJeuIA = joueur.getAdversaire().getBase();
        layoutBaseEnJeuIA.setWeightSum(enJeuIA.size());
        for(int i = 0; i< enJeuIA.size();i++){
            ImageView image = (ImageView)layoutBaseEnJeuIA.getChildAt(i);
            image.setTag(enJeuIA.get(i));
            image.setImageResource(enJeuIA.get(i).getIdImage());
        }
        // ON RAFFRAICHIT L'IMAGE DES CARTES QUE LE JOUEUR A EN MAIN
        // ON COMMENCE PAR EFFACER
        childCount = layoutMain.getChildCount();
        for(int i =0;i<childCount;i++){
            ImageView v = (ImageView)layoutMain.getChildAt(i);
            v.setImageResource(android.R.color.transparent);
        }
        // PUIS ON REMET CEUX QUE LE JOUEUR A EN MAIN
        List<Carte> main = joueur.getMain();
        layoutMain.setWeightSum(main.size());
        for(int i =0; i<main.size();i++){
            ImageView image = (ImageView)layoutMain.getChildAt(i);
            image.setTag(main.get(i));
            image.setImageResource((main.get(i).getIdImage()));
        }
    }

    // FONCTION UTILITAIRE PERMETTANT D'OBTENIR LE CODE DE L'IMAGE A PARTIR D'UN IMAGE CLIQUÉ
    private int getDrawableId(ImageView iv) {
        return ((Carte)iv.getTag()).getIdImage();
    }


    // FONCTION QUI VÉRIFIE SI LA PARTIE EST TERMINÉE
    public void checkEndGame(){
        // Il est plus rapide de regarder directement la vie que de faire appel a jeu.isFinPartie() car il ne change que lorsque qu'on appuie sur fin de tour
        if(joueur.getVie()<=0 || ia.getVie()<=0){
            Intent endGame = new Intent(GameActivity.this,EndGameActivity.class);

            if(joueur.getVie()>0){
                endGame.putExtra("gagnant",joueur.getNom());
                endGame.putExtra("tour",joueur.getTour());
                endGame.putExtra("vie",joueur.getVie());
            }
            else{
                endGame.putExtra("gagnant",ia.getNom());
                endGame.putExtra("tour",ia.getTour());
                endGame.putExtra("vie",ia.getVie());
            }

            startActivity(endGame);
            finish();
        }
    }

}
