package com.example.manuel.starrealms.Cards.Bases.Blob;

import com.example.manuel.starrealms.Cards.Modele.Base;
import com.example.manuel.starrealms.Cards.Modele.Carte;
import com.example.manuel.starrealms.Entity.Choix;
import com.example.manuel.starrealms.Entity.Faction;
import com.example.manuel.starrealms.Entity.IA;
import com.example.manuel.starrealms.Entity.Joueur;
import com.example.manuel.starrealms.R;


public class BlobWorld extends Base {

    public BlobWorld(){

        this.nom = "BlobWorld";
        this.faction = Faction.BLOB;
        this.idImage = R.drawable.blobworld;
        this.cout = 8;
        this.vie=7;
        this.abiliteeUtilise = false;
    }

    @Override
    public void jouerCarte(Joueur j) {
            if(j instanceof IA){
                Choix choix1 = new Choix(1,"Ajouter 5 dégats");
                Choix choix2 = new Choix(2,"Piger une carte pour chaque Blob joué ce tour-ci");

                ((IA)j).faireChoix(this, choix1,choix2);
            }
            else{
                j.faireChoixDegatPigeBlob();
            }
    }

    @Override
    public void choixFait(int choix, Joueur j){
        if(choix == 1){
            j.ajouteDegat(5);
        }
        else{
            int nombreBlob= j.compteCarteParType(j.getEnJeu(), Carte::isBlob);
            j.pigerCarte(nombreBlob);
        }
    }
}
