package com.example.manuel.starrealms.Cards.Modele;

// CLASSE PERMETTANT D'AJOUTER DE LA VIE ET UN MARQUEUR D'UTILISATION
// À UNE SIMPLE CARTE
public abstract class Base extends Carte {
    protected int vie;
    protected boolean utilisee;


    // GETTERS ET SETTERS
    public int getVie() {
        return vie;
    }

    public void setVie(int vie) {
        this.vie = vie;
    }

    public boolean isUtilisee() {
        return utilisee;
    }

    public void setUtilisee(boolean utilisee) {
        this.utilisee = utilisee;
    }
}
