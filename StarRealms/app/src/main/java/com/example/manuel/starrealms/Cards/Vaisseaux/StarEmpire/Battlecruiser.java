package com.example.manuel.starrealms.Cards.Vaisseaux.StarEmpire;

import com.example.manuel.starrealms.Cards.Modele.Base;
import com.example.manuel.starrealms.Cards.Modele.CarteFeraille;
import com.example.manuel.starrealms.Cards.Modele.Vaisseau;
import com.example.manuel.starrealms.Cards.Modele.CarteAlliee;
import com.example.manuel.starrealms.Entity.Faction;
import com.example.manuel.starrealms.Entity.IA;
import com.example.manuel.starrealms.Entity.Joueur;
import com.example.manuel.starrealms.R;


public class Battlecruiser extends Vaisseau implements CarteAlliee,CarteFeraille {

    public Battlecruiser(){

        this.nom = "Battlecruiser";
        this.faction = Faction.STAR_EMPIRE;
        this.idImage = R.drawable.battlecruiser;
        this.cout = 6;
        this.abiliteeUtilise = false;
        this.abiliteeAllieUtilise = false;
    }

    @Override
    public void alliee(Joueur j) {
        if(j instanceof IA){
            j.faireChoixDefausse((IA)j);
        }
        else{
            j.adversaireDefausse();
        }
    }

    @Override
    public void ferailler(Joueur j) {
        j.pigerCarte(1);
        if(j instanceof IA){
            ((IA)j).choisirBaseADetruire();
        }
        else{
            j.faireChoixDetruireBase();
        }

    }


    @Override
    public void jouerCarte(Joueur j) {
        j.ajouteDegat(5);
        j.pigerCarte(1);
    }

    @Override
    public boolean detruireBaseFeraille(){
        return true;
    }
}
