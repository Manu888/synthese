package com.example.manuel.starrealms.Cards.Modele;
import com.example.manuel.starrealms.Entity.Joueur;

// CLASSE QUI PERMET D'AJOUTER LA METHODÉ ALLIÉE AUX CARTES
// L'IMPLÉMENTANT
public interface CarteAlliee  {

    void alliee(Joueur j);

}
