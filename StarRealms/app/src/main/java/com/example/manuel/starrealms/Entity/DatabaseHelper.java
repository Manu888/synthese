package com.example.manuel.starrealms.Entity;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {

    // Quand on installe l'application, on créer une base de donnée locale se nommant db
    public DatabaseHelper(Context context) {
        super(context, "db", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // APPELER QU'UNE SEULE FOIS LORSQUON INSTALLE L'APP !
        // On exécute la création de la table lors de la première installation
        db.execSQL("CREATE TABLE halloffame ( _id INTEGER PRIMARY KEY AUTOINCREMENT, nom TEXT, tour INTEGER, vie INTEGER );");

        // AJOUT D'UN ENTRÉE TEST DANS LA BD
        //ajouterJoueur("Manuel", 25,15,db);
        //ajouterJoueur("Philippe", 20,11,db);
        //ajouterJoueur("J-P", 18,10,db);
        //ajouterJoueur("Jordy", 18,17,db);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void ajouterJoueur(String nomJoueur,int nbTour, int nbVie, SQLiteDatabase db){
        // Fonction utilitaire afin d'ajouter des joueurs rapidement à la BD
        ContentValues temp = new ContentValues();
        temp.put("nom",nomJoueur);
        temp.put("tour",nbTour);
        temp.put("vie",nbVie);
        db.insert("halloffame",null,temp);
    }
}
