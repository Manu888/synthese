package com.example.manuel.starrealms.Cards.Vaisseaux;

import com.example.manuel.starrealms.Cards.Modele.Vaisseau;
import com.example.manuel.starrealms.Entity.Faction;
import com.example.manuel.starrealms.Entity.Joueur;
import com.example.manuel.starrealms.R;


public class Scout extends Vaisseau {

    public Scout(){
        this.nom = "Scout";
        this.faction = Faction.AUCUNE;
        this.idImage = R.drawable.scout;
        this.cout = 0;
        this.abiliteeUtilise = false;
        this.abiliteeAllieUtilise = false;
    }

    @Override
    public void jouerCarte(Joueur j) {
        j.ajouteOr(1);
    }
}
