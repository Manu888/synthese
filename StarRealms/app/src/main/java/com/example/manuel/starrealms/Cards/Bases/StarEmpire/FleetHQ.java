package com.example.manuel.starrealms.Cards.Bases.StarEmpire;

import com.example.manuel.starrealms.Cards.Modele.Base;
import com.example.manuel.starrealms.Entity.Faction;
import com.example.manuel.starrealms.Entity.Joueur;
import com.example.manuel.starrealms.R;


public class FleetHQ extends Base {

    public FleetHQ(){

        this.nom = "FleetHQ";
        this.faction = Faction.STAR_EMPIRE;
        this.idImage = R.drawable.fleethq;
        this.cout = 8;
        this.vie = 8;
        this.abiliteeUtilise = false;
    }

    @Override
    public void jouerCarte(Joueur j) {
        j.setAjouterDegatChqVaisseau(true);
    }
}
