package com.example.manuel.starrealms.Cards.Bases.StarEmpire;

import com.example.manuel.starrealms.Cards.Modele.AvantPoste;
import com.example.manuel.starrealms.Cards.Modele.CarteAlliee;
import com.example.manuel.starrealms.Entity.Faction;
import com.example.manuel.starrealms.Entity.IA;
import com.example.manuel.starrealms.Entity.Joueur;
import com.example.manuel.starrealms.R;


public class RoyalRedoubt extends AvantPoste implements CarteAlliee {

    public RoyalRedoubt(){

        this.nom = "RoyalRedoubt";
        this.faction = Faction.STAR_EMPIRE;
        this.idImage = R.drawable.royalredoubt;
        this.cout = 6;
        this.vie = 6;
        this.abiliteeUtilise = false;
        this.abiliteeAllieUtilise = false;
    }

    @Override
    public void alliee(Joueur j) {
        if(j instanceof IA){
            j.faireChoixDefausse((IA)j);
        }
        else{
            j.adversaireDefausse();
        }
    }

    @Override
    public void jouerCarte(Joueur j) {
            j.ajouteDegat(3);
    }
}
