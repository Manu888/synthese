package com.example.manuel.starrealms.Interface;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.example.manuel.starrealms.Cards.Modele.Carte;
import com.example.manuel.starrealms.Cards.Modele.CarteFeraille;

import com.example.manuel.starrealms.R;


public class EnJeuFragment extends Fragment {

    ImageView image;
    Button boutonFerrailler;
    Ecouteur e;
    Carte c ;


    public EnJeuFragment() {
        // Required empty public constructor
    }

    public static EnJeuFragment newInstance(Carte c) {
        EnJeuFragment fragment = new EnJeuFragment();
        Bundle args = new Bundle();
        // On associe l'image de la carte à afficher ainsi que la carte au fragment
        int idImage = c.getIdImage();
        args.putInt("idImage",idImage);
        fragment.setArguments(args);
        args.putSerializable("carte",c);

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup)inflater.inflate(R.layout.fragment_en_jeu, container, false);
        c =(Carte) this.getArguments().get("carte");
        // On affiche l'image de la carte
        image = (ImageView)rootView.findViewById(R.id.imageDefausse);
        image.setImageResource((Integer)c.getIdImage());
        // On associe un ecouteur au bouton de féraille
        boutonFerrailler = (Button)rootView.findViewById(R.id.ferrailler);
        e = new Ecouteur();
        boutonFerrailler.setOnClickListener(e);

        // Si la carte est possible d'être férailler, on affiche le bouton pour le faire
        if(c instanceof CarteFeraille){
            boutonFerrailler.setVisibility(View.VISIBLE);
        }

        return rootView;
    }

    public class Ecouteur implements View.OnClickListener{

        @Override
        public void onClick(View view) {

            if(view == boutonFerrailler){
                // Si on veut ferailler la carte, on revien au jeu pour le faire
                // en retournant la carte en question
                Intent i = new Intent();
                i.putExtra("carte",c);
                getActivity().setResult(EnJeuActivity.RESULT_OK, i);
                getActivity().onBackPressed();
            }

        }
    }

}
