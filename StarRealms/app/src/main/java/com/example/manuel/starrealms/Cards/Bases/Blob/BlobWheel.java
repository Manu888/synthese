package com.example.manuel.starrealms.Cards.Bases.Blob;

import com.example.manuel.starrealms.Cards.Modele.Base;
import com.example.manuel.starrealms.Cards.Modele.CarteFeraille;
import com.example.manuel.starrealms.Entity.Faction;
import com.example.manuel.starrealms.Entity.Joueur;
import com.example.manuel.starrealms.R;


public class BlobWheel extends Base implements CarteFeraille {

    public BlobWheel(){

        this.nom = "BlobWheel";
        this.faction = Faction.BLOB;
        this.idImage = R.drawable.blobwheel;
        this.cout = 3;
        this.vie=5;
        this.abiliteeUtilise = false;

    }

    @Override
    public void ferailler(Joueur j) {
            j.ajouteOr(3);
    }

    @Override
    public void jouerCarte(Joueur j) {
            j.ajouteDegat(1);
    }
    @Override
    public int ajouteOrFeraille(){
        return 3;
    }
}
