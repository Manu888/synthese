package com.example.manuel.starrealms.Cards.Vaisseaux.MachineCult;

import com.example.manuel.starrealms.Cards.Modele.Carte;
import com.example.manuel.starrealms.Cards.Modele.Vaisseau;
import com.example.manuel.starrealms.Cards.Modele.CarteAlliee;
import com.example.manuel.starrealms.Entity.Choix;
import com.example.manuel.starrealms.Entity.Faction;
import com.example.manuel.starrealms.Entity.IA;
import com.example.manuel.starrealms.Entity.Joueur;
import com.example.manuel.starrealms.R;


public class PatrolMech extends Vaisseau implements CarteAlliee {

    public PatrolMech(){

        this.nom = "PatrolMech";
        this.faction = Faction.MACHINE_CULT;
        this.idImage = R.drawable.patrolmech;
        this.cout = 4;
        this.abiliteeUtilise = false;
        this.abiliteeAllieUtilise = false;
    }

    @Override
    public void alliee(Joueur j) {
        if(j instanceof IA){
            ((IA)j).feraillerCarteDeMainOuDefausse(1);
        }
        else{
            j.faireChoixFerraillerMainDefausse();
        }
    }

    @Override
    public void jouerCarte(Joueur j) {
        if(j instanceof IA) {
            Choix choix1 = new Choix(1,"Ajouter 3 or");
            Choix choix2 = new Choix(2,"Ajouter 5 dégats");

            ((IA)j).faireChoix(this,choix1,choix2);
        }
        else {
            j.faireChoixOrDegat();
        }
    }

    @Override
    public void choixFait(int choix, Joueur j){
        if(choix == 1) {
            j.ajouteOr(3);
        }
        else{
            j.ajouteDegat(5);
        }
    }

}
