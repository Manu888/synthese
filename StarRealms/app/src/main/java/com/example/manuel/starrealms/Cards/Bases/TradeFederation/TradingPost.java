package com.example.manuel.starrealms.Cards.Bases.TradeFederation;

import com.example.manuel.starrealms.Cards.Modele.AvantPoste;
import com.example.manuel.starrealms.Cards.Modele.CarteFeraille;
import com.example.manuel.starrealms.Entity.Choix;
import com.example.manuel.starrealms.Entity.Faction;
import com.example.manuel.starrealms.Entity.IA;
import com.example.manuel.starrealms.Entity.Joueur;
import com.example.manuel.starrealms.R;


public class TradingPost extends AvantPoste implements CarteFeraille {

    public TradingPost(){

        this.nom = "TradingPost";
        this.faction = Faction.TRADE_FEDERATION;
        this.idImage = R.drawable.tradingpost;
        this.cout = 3;
        this.vie=4;
        this.abiliteeUtilise = false;
    }

    @Override
    public void ferailler(Joueur j) {
            j.ajouteDegat(3);
    }

    @Override
    public void jouerCarte(Joueur j) {
        if(j instanceof IA){
            Choix choix1 = new Choix(1,"Ajouter 1 vie");
            Choix choix2 = new Choix(2,"Ajouter 1 or");

            ((IA)j).faireChoix(this,choix1,choix2);
        }
        else{
            j.faireChoixVieOr();
        }
    }

    @Override
    public int ajouteCombatFeraille(){
        return 3;
    }
    @Override
    public void choixFait(int choix, Joueur j){
        if(choix == 1){
            j.ajouteVie(1);
        }
        else{
            j.ajouteOr(1);
        }
    }
}
