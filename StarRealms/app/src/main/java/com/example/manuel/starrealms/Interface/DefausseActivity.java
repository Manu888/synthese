package com.example.manuel.starrealms.Interface;

import android.content.Context;
import android.content.Intent;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.example.manuel.starrealms.R;

import java.util.ArrayList;

public class DefausseActivity extends AppCompatActivity {

    private ViewPager pager;
    private SwipeAdapter adapter;
    private ArrayList<Integer> listeCarte ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_defausse);

        // On récupère le pager afin de lui associer le SwipeAdapter
        pager = (ViewPager)findViewById(R.id.pager);
        Intent i = getIntent();
        listeCarte = i.getIntegerArrayListExtra("defausse");
        adapter = new SwipeAdapter(getSupportFragmentManager());
        pager.setAdapter(adapter);

    }

    public class SwipeAdapter extends FragmentStatePagerAdapter{

        public SwipeAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // À chaque fragment,on récupere la carte correspondante dans la liste de carte
            return DefausseFragment.newInstance(listeCarte.get(position));
        }

        @Override
        public int getCount() {
            // Il y a autant de fragments que de carte dans la liste de carte à montrer
            return listeCarte.size();
        }
    }
}
