package com.example.manuel.starrealms.Cards.Bases.Blob;

import com.example.manuel.starrealms.Cards.Modele.Base;
import com.example.manuel.starrealms.Cards.Modele.CarteAlliee;
import com.example.manuel.starrealms.Entity.Faction;
import com.example.manuel.starrealms.Entity.Joueur;
import com.example.manuel.starrealms.R;


public class TheHive extends Base implements CarteAlliee {

    public TheHive(){

        this.nom = "TheHive";
        this.faction = Faction.BLOB;
        this.idImage = R.drawable.thehive;
        this.cout = 5;
        this.vie = 5;
        this.abiliteeUtilise = false;
        this.abiliteeAllieUtilise = false;
    }

    @Override
    public void alliee(Joueur j) {
            j.pigerCarte(1);
    }

    @Override
    public void jouerCarte(Joueur j) {
            j.ajouteDegat(5);
    }
}
