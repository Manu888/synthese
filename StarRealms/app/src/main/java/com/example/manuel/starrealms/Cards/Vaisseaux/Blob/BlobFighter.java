package com.example.manuel.starrealms.Cards.Vaisseaux.Blob;

import com.example.manuel.starrealms.Cards.Modele.Vaisseau;
import com.example.manuel.starrealms.Cards.Modele.CarteAlliee;
import com.example.manuel.starrealms.Entity.Faction;
import com.example.manuel.starrealms.Entity.Joueur;
import com.example.manuel.starrealms.R;


public class BlobFighter extends Vaisseau implements CarteAlliee {

    public BlobFighter(){

        this.nom = "BlobFighter";
        this.faction = Faction.BLOB;
        this.idImage = R.drawable.blobfighter;
        this.cout = 1;
        this.abiliteeUtilise = false;
        this.abiliteeAllieUtilise = false;
    }

    @Override
    public void alliee(Joueur j) {
        j.pigerCarte(1);
    }

    @Override
    public void jouerCarte(Joueur j) {
        j.ajouteDegat(3);
    }
}
