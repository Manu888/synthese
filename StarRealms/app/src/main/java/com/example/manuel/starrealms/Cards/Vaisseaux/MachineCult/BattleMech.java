package com.example.manuel.starrealms.Cards.Vaisseaux.MachineCult;

import com.example.manuel.starrealms.Cards.Modele.Carte;
import com.example.manuel.starrealms.Cards.Modele.Vaisseau;
import com.example.manuel.starrealms.Cards.Modele.CarteAlliee;
import com.example.manuel.starrealms.Entity.Faction;
import com.example.manuel.starrealms.Entity.IA;
import com.example.manuel.starrealms.Entity.Joueur;
import com.example.manuel.starrealms.R;


public class BattleMech extends Vaisseau implements CarteAlliee {

    public BattleMech(){

        this.nom = "BattleMech";
        this.faction = Faction.MACHINE_CULT;
        this.idImage = R.drawable.battlemech;
        this.cout = 5;
        this.abiliteeUtilise = false;
        this.abiliteeAllieUtilise = false;
    }

    @Override
    public void alliee(Joueur j) {
            j.pigerCarte(1);
    }

    @Override
    public void jouerCarte(Joueur j) {
        j.ajouteDegat(4);
        if(j instanceof IA){
            ((IA)j).feraillerCarteDeMainOuDefausse(1);
        }
        else{
            j.faireChoixFerraillerMainDefausse();
        }

    }

}
