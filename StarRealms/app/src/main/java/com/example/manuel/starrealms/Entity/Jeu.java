package com.example.manuel.starrealms.Entity;


import android.support.v7.app.AppCompatActivity;

import com.example.manuel.starrealms.Cards.Bases.Blob.BlobWheel;
import com.example.manuel.starrealms.Cards.Bases.Blob.BlobWorld;
import com.example.manuel.starrealms.Cards.Bases.Blob.TheHive;
import com.example.manuel.starrealms.Cards.Bases.MachineCult.BattleStation;
import com.example.manuel.starrealms.Cards.Bases.MachineCult.BrainWorld;
import com.example.manuel.starrealms.Cards.Bases.MachineCult.Junkyard;
import com.example.manuel.starrealms.Cards.Bases.MachineCult.MachineBase;
import com.example.manuel.starrealms.Cards.Bases.MachineCult.MechWorld;
import com.example.manuel.starrealms.Cards.Bases.StarEmpire.FleetHQ;
import com.example.manuel.starrealms.Cards.Bases.StarEmpire.RecyclingStation;
import com.example.manuel.starrealms.Cards.Bases.StarEmpire.RoyalRedoubt;
import com.example.manuel.starrealms.Cards.Bases.StarEmpire.SpaceStation;
import com.example.manuel.starrealms.Cards.Bases.StarEmpire.WarWorld;
import com.example.manuel.starrealms.Cards.Bases.TradeFederation.BarterWorld;
import com.example.manuel.starrealms.Cards.Bases.TradeFederation.CentralOffice;
import com.example.manuel.starrealms.Cards.Bases.TradeFederation.DefenseCenter;
import com.example.manuel.starrealms.Cards.Bases.TradeFederation.PortOfCall;
import com.example.manuel.starrealms.Cards.Bases.TradeFederation.TradingPost;
import com.example.manuel.starrealms.Cards.Modele.Carte;
import com.example.manuel.starrealms.Cards.Vaisseaux.Blob.BattleBlob;
import com.example.manuel.starrealms.Cards.Vaisseaux.Blob.BattlePod;
import com.example.manuel.starrealms.Cards.Vaisseaux.Blob.BlobCarrier;
import com.example.manuel.starrealms.Cards.Vaisseaux.Blob.BlobDestroyer;
import com.example.manuel.starrealms.Cards.Vaisseaux.Blob.BlobFighter;
import com.example.manuel.starrealms.Cards.Vaisseaux.Blob.Mothership;
import com.example.manuel.starrealms.Cards.Vaisseaux.Blob.Ram;
import com.example.manuel.starrealms.Cards.Vaisseaux.Blob.TradePod;
import com.example.manuel.starrealms.Cards.Vaisseaux.Explorer;
import com.example.manuel.starrealms.Cards.Vaisseaux.MachineCult.BattleMech;
import com.example.manuel.starrealms.Cards.Vaisseaux.MachineCult.MissileBot;
import com.example.manuel.starrealms.Cards.Vaisseaux.MachineCult.MissileMech;
import com.example.manuel.starrealms.Cards.Vaisseaux.MachineCult.PatrolMech;
import com.example.manuel.starrealms.Cards.Vaisseaux.MachineCult.StealthNeedle;
import com.example.manuel.starrealms.Cards.Vaisseaux.MachineCult.SupplyBot;
import com.example.manuel.starrealms.Cards.Vaisseaux.MachineCult.TradeBot;
import com.example.manuel.starrealms.Cards.Vaisseaux.Scout;
import com.example.manuel.starrealms.Cards.Vaisseaux.StarEmpire.Battlecruiser;
import com.example.manuel.starrealms.Cards.Vaisseaux.StarEmpire.Corvette;
import com.example.manuel.starrealms.Cards.Vaisseaux.StarEmpire.Dreadnaught;
import com.example.manuel.starrealms.Cards.Vaisseaux.StarEmpire.ImperialFighter;
import com.example.manuel.starrealms.Cards.Vaisseaux.StarEmpire.ImperialFrigate;
import com.example.manuel.starrealms.Cards.Vaisseaux.StarEmpire.SurveyShip;
import com.example.manuel.starrealms.Cards.Vaisseaux.TradeFederation.CommandShip;
import com.example.manuel.starrealms.Cards.Vaisseaux.TradeFederation.Cutter;
import com.example.manuel.starrealms.Cards.Vaisseaux.TradeFederation.EmbassyYacht;
import com.example.manuel.starrealms.Cards.Vaisseaux.TradeFederation.FederationShuttle;
import com.example.manuel.starrealms.Cards.Vaisseaux.TradeFederation.Flagship;
import com.example.manuel.starrealms.Cards.Vaisseaux.TradeFederation.Freighter;
import com.example.manuel.starrealms.Cards.Vaisseaux.TradeFederation.TradeEscort;
import com.example.manuel.starrealms.Cards.Vaisseaux.Viper;
import com.example.manuel.starrealms.Interface.GameActivity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Jeu {

    private int tour;
    private List<Joueur> joueurs;
    private List<Carte> deck;
    private List<Carte> marche;
    private Explorer explorer;
    private int indexJoueurCourant;
    private boolean finPartie;
    private Joueur joueur;
    private IA ia;


    // GETTERS ET SETTERS

    public int getTour() {
        return tour;
    }

    public void setTour(int tour) {
        this.tour = tour;
    }

    public List<Joueur> getJoueurs() {
        return joueurs;
    }

    public void setJoueurs(List<Joueur> joueurs) {
        this.joueurs = joueurs;
    }

    public List<Carte> getDeck() {
        return deck;
    }

    public void setDeck(List<Carte> deck) {
        this.deck = deck;
    }

    public List<Carte> getMarche() {
        return marche;
    }

    public void setMarche(List<Carte> marche) {
        this.marche = marche;
    }

    public Explorer getExplorer() {
        return explorer;
    }

    public void setExplorer(Explorer explorer) {
        this.explorer = explorer;
    }

    public int getIndexJoueurCourant() {
        return indexJoueurCourant;
    }

    public void setIndexJoueurCourant(int indexJoueurCourant) {
        this.indexJoueurCourant = indexJoueurCourant;
    }

    public boolean isFinPartie() {
        return finPartie;
    }

    public void setFinPartie(boolean finPartie) {
        this.finPartie = finPartie;
    }

    public Joueur getJoueur() {
        return joueur;
    }

    public void setJoueur(Joueur joueur) {
        this.joueur = joueur;
    }

    public IA getIa() {
        return ia;
    }

    public void setIa(IA ia) {
        this.ia = ia;
    }

    public Joueur getGagnant(){
        return joueurs.stream().max((j1,j2)->Integer.compare(j1.getVie(),j2.getVie())).get();
    }
    public Joueur getPerdant(){
        return joueurs.stream().min((j1,j2)->Integer.compare(j1.getVie(),j2.getVie())).get();
    }

    public Joueur getJoueurCourant(){
        return joueurs.get(indexJoueurCourant);
    }

    public Carte getCarteDuMarche(String nom){
        Carte carteDuMarche = null;

        for(Carte carte : marche){
            if(carte.getNom().equals(nom))
                carteDuMarche = carte;
        }

        return carteDuMarche;
    }
    // ###########################################   MÉTHODE DE JEU  #########################################################

    // Fonction qui permet d'ajouter de nouvelle cartes au marché
    public void ajouteNombreCarteAuMarche(int nbCarte){
        for(int i = 0; i< nbCarte;i++){
            if(!deck.isEmpty()){
                ajouteCarteAuMarche(deck.remove(0));
            }

        }
    }

    // Ajoute une carte spécifique au marché
    public void ajouteCarteAuMarche(Carte c){
        marche.add(c);
    }

    // Permet de ferrailler une carte spécifique du marché
    public void ferrailleCarteDuMarche(Carte c){
        marche.remove(c);
        ajouteNombreCarteAuMarche(1);
    }

    // Fonction qui débute la partie pour les joueurs
    public void debutePartie(){
        indexJoueurCourant = 0;
        tour = 1;
        joueurs.get(0).pigerCarte(3);
        joueurs.get(1).pigerCarte(5);
    }

    // Fonction qui fait progresser le tour
    public void tourFini(){
        for(Joueur j : joueurs)
            if(j.getVie()<=0){
                finPartie=true;
                System.out.println("##################PARTIE FINI #################################");
                return;
        }
        if(indexJoueurCourant == joueurs.size()-1)
            indexJoueurCourant = 0;
        else
            indexJoueurCourant++;

        tour++;
    }

    // Fonction d'initialisation de partie, mise en place de tous les éléments de jeu
    public void initialiserPartie(GameActivity act){
        List<Carte> deck = new ArrayList<>();

        deck.addAll(getCoreSetDeck());

        setDeck(deck);

        setExplorer(new Explorer());

        Collections.shuffle(getDeck());

        List<Joueur> listeJoueur = new ArrayList<>();
        joueur = new Joueur(act);
        listeJoueur.add(joueur);
        ia = new IA(act);
        listeJoueur.add(ia);

        setJoueurs(listeJoueur);

        Collections.shuffle(getJoueurs());

        Joueur j1 = joueurs.get(0);
        j1.setDebute(true);
        Joueur j2 = joueurs.get(1);
        j2.setDebute(false);

        j1.setAdversaire(j2);
        j2.setAdversaire(j1);

        j1.setNbBrasse(0);
        j2.setNbBrasse(0);

        j1.setTour(1);
        j2.setTour(1);

        j1.setVie(50);
        j2.setVie(50);


        j1.setDeck(getCarteBase());
        j2.setDeck(getCarteBase());

        j1.setJeu(this);
        j2.setJeu(this);

        Collections.shuffle(j1.getDeck());
        Collections.shuffle(j2.getDeck());


        List<Carte> marche = new ArrayList<>();
        setMarche(marche);
        ajouteNombreCarteAuMarche(5);
        debutePartie();
    }

    // Fonction qui permet de créer un deck de carte de début de partie
    public  List<Carte> getCarteBase(){
        List<Carte> deck = new ArrayList<>();

        deck.add(new Scout());
        deck.add(new Scout());
        deck.add(new Scout());
        deck.add(new Scout());
        deck.add(new Scout());
        deck.add(new Scout());
        deck.add(new Scout());
        deck.add(new Scout());
        deck.add(new Viper());
        deck.add(new Viper());

        return deck;
    }

    // Fonction qui permet d'obtenir le deck de toutes les cartes du jeu original
    public List<Carte> getCoreSetDeck(){

        List<Carte> deck = new ArrayList<>();

        // Ajout des cartes MACHINE CULT

        deck.add(new TradeBot());
        deck.add(new TradeBot());
        deck.add(new TradeBot());
        deck.add(new MissileBot());
        deck.add(new MissileBot());
        deck.add(new MissileBot());
        deck.add(new SupplyBot());
        deck.add(new SupplyBot());
        deck.add(new SupplyBot());
        deck.add(new PatrolMech());
        deck.add(new PatrolMech());
        deck.add(new StealthNeedle());
        deck.add(new BattleMech());
        deck.add(new MissileMech());
        deck.add(new BattleStation());
        deck.add(new BattleStation());
        deck.add(new MechWorld());
        deck.add(new BrainWorld());
        deck.add(new MachineBase());
        deck.add(new Junkyard());

        // AJOUT DES CARTES STAR EMPIRE
        deck.add(new ImperialFighter());
        deck.add(new ImperialFighter());
        deck.add(new ImperialFighter());
        deck.add(new ImperialFrigate());
        deck.add(new ImperialFrigate());
        deck.add(new ImperialFrigate());
        deck.add(new SurveyShip());
        deck.add(new SurveyShip());
        deck.add(new SurveyShip());
        deck.add(new Corvette());
        deck.add(new Corvette());
        deck.add(new Battlecruiser());
        deck.add(new Dreadnaught());
        deck.add(new SpaceStation());
        deck.add(new SpaceStation());
        deck.add(new RecyclingStation());
        deck.add(new RecyclingStation());
        deck.add(new WarWorld());
        deck.add(new RoyalRedoubt());
        deck.add(new FleetHQ());

        // AJOUT DES CARTES FEDERATION EMPIRE
        deck.add(new FederationShuttle());
        deck.add(new FederationShuttle());
        deck.add(new FederationShuttle());
        deck.add(new Cutter());
        deck.add(new Cutter());
        deck.add(new Cutter());
        deck.add(new EmbassyYacht());
        deck.add(new EmbassyYacht());
        deck.add(new Freighter());
        deck.add(new Freighter());
        deck.add(new CommandShip());
        deck.add(new TradeEscort());
        deck.add(new Flagship());
        deck.add(new TradingPost());
        deck.add(new TradingPost());
        deck.add(new BarterWorld());
        deck.add(new BarterWorld());
        deck.add(new DefenseCenter());
        deck.add(new CentralOffice());
        deck.add(new PortOfCall());

        // Ajout des cartes BLOB
        deck.add(new BlobFighter());
        deck.add(new BlobFighter());
        deck.add(new BlobFighter());
        deck.add(new TradePod());
        deck.add(new TradePod());
        deck.add(new TradePod());
        deck.add(new BattlePod());
        deck.add(new BattlePod());
        deck.add(new Ram());
        deck.add(new Ram());
        deck.add(new BlobDestroyer());
        deck.add(new BlobDestroyer());
        deck.add(new BattleBlob());
        deck.add(new BlobCarrier());
        deck.add(new Mothership());
        deck.add(new BlobWheel());
        deck.add(new BlobWheel());
        deck.add(new BlobWheel());
        deck.add(new TheHive());
        deck.add(new BlobWorld());

        return deck;
    }

}
