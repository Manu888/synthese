package com.example.manuel.starrealms.Cards.Bases.TradeFederation;

import com.example.manuel.starrealms.Cards.Modele.AvantPoste;
import com.example.manuel.starrealms.Cards.Modele.CarteFeraille;
import com.example.manuel.starrealms.Entity.Faction;
import com.example.manuel.starrealms.Entity.IA;
import com.example.manuel.starrealms.Entity.Joueur;
import com.example.manuel.starrealms.R;


public class PortOfCall extends AvantPoste implements CarteFeraille {

    public PortOfCall(){

        this.nom = "PortOfCall";
        this.faction = Faction.TRADE_FEDERATION;
        this.idImage = R.drawable.portofcall;
        this.cout = 6;
        this.vie = 6;
        this.abiliteeUtilise = false;
    }

    @Override
    public void ferailler(Joueur j) {
        j.pigerCarte(1);
        if(j instanceof IA){
            ((IA)j).choisirBaseADetruire();
        }
        else{
            j.faireChoixDetruireBase();
        }


    }

    @Override
    public void jouerCarte(Joueur j) {
        j.ajouteOr(3);
    }

    @Override
    public boolean detruireBaseFeraille(){
        return true;
    }
}
