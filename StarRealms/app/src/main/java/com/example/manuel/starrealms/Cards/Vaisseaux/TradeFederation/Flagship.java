package com.example.manuel.starrealms.Cards.Vaisseaux.TradeFederation;

import com.example.manuel.starrealms.Cards.Modele.Vaisseau;
import com.example.manuel.starrealms.Cards.Modele.CarteAlliee;
import com.example.manuel.starrealms.Entity.Faction;
import com.example.manuel.starrealms.Entity.Joueur;
import com.example.manuel.starrealms.R;


public class Flagship extends Vaisseau implements CarteAlliee {

    public Flagship(){

        this.nom = "Flagship";
        this.faction = Faction.TRADE_FEDERATION;
        this.idImage = R.drawable.flagship;
        this.cout = 6;
        this.abiliteeUtilise = false;
        this.abiliteeAllieUtilise = false;
    }

    @Override
    public void alliee(Joueur j) {
            j.ajouteVie(5);
    }

    @Override
    public void jouerCarte(Joueur j) {
            j.ajouteDegat(5);
            j.pigerCarte(1);
    }
}
