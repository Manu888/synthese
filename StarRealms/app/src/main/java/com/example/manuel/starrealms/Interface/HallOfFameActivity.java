package com.example.manuel.starrealms.Interface;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.example.manuel.starrealms.Entity.EnregistrementBD;
import com.example.manuel.starrealms.Entity.Operation;
import com.example.manuel.starrealms.R;

import java.util.Vector;

public class HallOfFameActivity extends AppCompatActivity {

    ListView listeJoueur;
    Operation op;
    ArrayAdapter<String> adapteur;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hall_of_fame);

        // On récupère les élements de l'activité
        listeJoueur = (ListView)findViewById(R.id.listeHall);
        LinearLayout hall = (LinearLayout)findViewById(R.id.hall);
        hall.setBackgroundColor(Color.BLACK);

        // On ouvre la BD pour récupérer les meilleurs joueurs enregistrés
        op = new Operation(HallOfFameActivity.this);
        op.ouvrir();
        adapteur = new ArrayAdapter<String>(HallOfFameActivity.this, android.R.layout.simple_list_item_1){
            @Override
            public View getView(int position, View convertView, ViewGroup parent){
                // On récupère la vue
                View view = super.getView(position, convertView, parent);

                //On initialise un textView pour chaque élément de la liste
                TextView tv = (TextView) view.findViewById(android.R.id.text1);

                // On change la couleur du texte
                tv.setTextColor(Color.CYAN);


                // on retourne la vue
                return view;
            }
        };

        // On récupère les 10 meilleures joueurs de la BD
        Vector<EnregistrementBD> joueurs = op.toutJoueurs();

        // Pour chaque joueur , on l'ajoute à l'adapteur
        for(int i= 0; i<joueurs.size(); i++){
            // On récupere le nom des bières trouvées
            adapteur.add("#"+(i+1)+"     -     "+joueurs.get(i).getNom()+"      -       "+ joueurs.get(i).getTour()+"         -         "+joueurs.get(i).getVie());
        }
        // On associe l'adapteur à la ListView puis on ferme la BD
        listeJoueur.setAdapter(adapteur);
        op.fermer();
    }
}
