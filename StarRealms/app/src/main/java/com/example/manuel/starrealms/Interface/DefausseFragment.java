package com.example.manuel.starrealms.Interface;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.manuel.starrealms.R;


public class DefausseFragment extends Fragment {

    ImageView image;

    public DefausseFragment() {
        // Required empty public constructor
    }

    public static DefausseFragment newInstance(Integer idImage) {
        // À chaque fragment, on recoit le code pour l'image à afficher et
        // on l'associe au fragment
        DefausseFragment fragment = new DefausseFragment();
        Bundle args = new Bundle();
        args.putInt("idImage",idImage);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // On affiche l'image qu'on a reçu dans l'ImageView
        ViewGroup rootView = (ViewGroup)inflater.inflate(R.layout.fragment_defausse, container, false);
        image = (ImageView)rootView.findViewById(R.id.imageDefausse);
        image.setImageResource((Integer)this.getArguments().get("idImage"));

        return rootView;
    }

}
