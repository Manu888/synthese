package com.example.manuel.starrealms.Entity;

// CLASSE GÉNÉRALE QUI PERMET DE SIMPLIFIER LES CHOIX FAIT PAR L'IA
public class Choix {

    private int numero;
    private String description;

    public Choix (int choix, String desc){
        this.numero = choix;
        this.description = desc;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
