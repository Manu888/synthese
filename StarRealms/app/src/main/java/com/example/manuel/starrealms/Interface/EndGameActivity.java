package com.example.manuel.starrealms.Interface;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.manuel.starrealms.Entity.Operation;
import com.example.manuel.starrealms.R;

public class EndGameActivity extends AppCompatActivity {

    TextView gagnant;
    Button enregistrer, retour;
    String nom;
    int tour, vie;
    Operation op;
    Ecouteur ec;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_end_game);
        // On récupère les infos qui doivent être afficher
        gagnant = (TextView)findViewById(R.id.annonceGagnant);
        enregistrer = (Button)findViewById(R.id.enregistrer);
        retour = (Button)findViewById(R.id.retour);
        Intent i = getIntent();
        nom = i.getStringExtra("gagnant");
        tour = i.getIntExtra("tour",0);
        vie = i.getIntExtra("vie",0);
        // On créer un Ecouteur et on l'associe aux boutons
        ec = new Ecouteur();
        enregistrer.setOnClickListener(ec);
        retour.setOnClickListener(ec);

        // On crée un objet d'opération de la BD
        op = new Operation(EndGameActivity.this);

        gagnant.setText("Le gagnant est "+ nom + "\n au tour #"+tour +"\n avec "+vie+" vie(s)");

        // Si l'IA à gagner, on cache le bouton enregistrer
        if(nom.equals("IA")){
            enregistrer.setVisibility(View.GONE);
        }

    }

    private class Ecouteur implements View.OnClickListener{

        @Override
        public void onClick(View view) {
            if(view == enregistrer){
                // On ouvre la BD pour enregistrer le joueur avec ses infos
                op.ouvrir();
                op.ajouterJoueurHallOfFame(nom,tour,vie);
                op.fermer();
                enregistrer.setVisibility(View.GONE);
            }
            else if(view == retour){
                // On ferme l'activité si on appuie sur Retour
                finish();
            }

        }
    }
}
