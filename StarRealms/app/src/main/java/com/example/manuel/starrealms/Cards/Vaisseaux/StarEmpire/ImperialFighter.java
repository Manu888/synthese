package com.example.manuel.starrealms.Cards.Vaisseaux.StarEmpire;

import com.example.manuel.starrealms.Cards.Modele.Vaisseau;
import com.example.manuel.starrealms.Cards.Modele.CarteAlliee;
import com.example.manuel.starrealms.Entity.Faction;
import com.example.manuel.starrealms.Entity.IA;
import com.example.manuel.starrealms.Entity.Joueur;
import com.example.manuel.starrealms.R;


public class ImperialFighter extends Vaisseau implements CarteAlliee {

    public ImperialFighter(){

        this.nom = "ImperialFighter";
        this.faction = Faction.STAR_EMPIRE;
        this.idImage = R.drawable.imperialfighter;
        this.cout = 1;
        this.abiliteeUtilise = false;
        this.abiliteeAllieUtilise = false;
    }

    @Override
    public void alliee(Joueur j) {
            j.ajouteDegat(2);
    }

    @Override
    public void jouerCarte(Joueur j) {
        j.ajouteDegat(2);
        if(j instanceof IA){
            j.faireChoixDefausse((IA)j);
        }
        else{
            j.adversaireDefausse();
        }

    }
}
