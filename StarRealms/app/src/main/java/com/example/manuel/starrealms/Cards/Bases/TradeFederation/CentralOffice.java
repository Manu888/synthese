package com.example.manuel.starrealms.Cards.Bases.TradeFederation;

import com.example.manuel.starrealms.Cards.Modele.Base;
import com.example.manuel.starrealms.Cards.Modele.CarteAlliee;
import com.example.manuel.starrealms.Entity.Faction;
import com.example.manuel.starrealms.Entity.Joueur;
import com.example.manuel.starrealms.R;


public class CentralOffice extends Base implements CarteAlliee {

    public CentralOffice(){

        this.nom = "CentralOffice";
        this.faction = Faction.TRADE_FEDERATION;
        this.idImage = R.drawable.centraloffice;
        this.cout = 7;
        this.vie = 6;
        this.abiliteeUtilise = false;
        this.abiliteeAllieUtilise = false;
    }

    @Override
    public void alliee(Joueur j) {
            j.pigerCarte(1);
    }

    @Override
    public void jouerCarte(Joueur j) {
            j.ajouteOr(2);
            j.setProchainVaisseauSurDeck(true);
    }
}
