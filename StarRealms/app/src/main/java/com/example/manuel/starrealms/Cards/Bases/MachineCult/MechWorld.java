package com.example.manuel.starrealms.Cards.Bases.MachineCult;

import com.example.manuel.starrealms.Cards.Modele.AvantPoste;
import com.example.manuel.starrealms.Entity.Faction;
import com.example.manuel.starrealms.Entity.Joueur;
import com.example.manuel.starrealms.R;


public class MechWorld extends AvantPoste {

    public MechWorld(){

        this.nom = "MechWorld";
        this.faction = Faction.MACHINE_CULT;
        this.idImage = R.drawable.mechworld;
        this.cout = 5;
        this.vie = 6;
        this.abiliteeUtilise = false;
    }

    @Override
    public void jouerCarte(Joueur j) {
        j.setTousFactionAlliee(true);

    }
}
