package com.example.manuel.starrealms.Cards.Bases.MachineCult;

import com.example.manuel.starrealms.Cards.Modele.AvantPoste;
import com.example.manuel.starrealms.Cards.Modele.CarteFeraille;
import com.example.manuel.starrealms.Entity.Faction;
import com.example.manuel.starrealms.Entity.Joueur;
import com.example.manuel.starrealms.R;


public class BattleStation extends AvantPoste implements CarteFeraille {

    public BattleStation(){

        this.nom = "BattleStation";
        this.faction = Faction.MACHINE_CULT;
        this.idImage = R.drawable.battlestation;
        this.cout = 3;
        this.vie=5;
        this.abiliteeUtilise = false;
    }

    @Override
    public void ferailler(Joueur j) {
        j.ajouteDegat(5);
    }

    @Override
    public void jouerCarte(Joueur j) {

    }

    @Override
    public int ajouteCombatFeraille(){
        return 5;
    }
}
