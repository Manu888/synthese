package com.example.manuel.starrealms.Cards.Vaisseaux.Blob;

import com.example.manuel.starrealms.Cards.Modele.CarteAlliee;
import com.example.manuel.starrealms.Cards.Modele.Vaisseau;
import com.example.manuel.starrealms.Cards.Modele.CarteFeraille;
import com.example.manuel.starrealms.Entity.Faction;
import com.example.manuel.starrealms.Entity.Joueur;
import com.example.manuel.starrealms.R;


public class Ram extends Vaisseau implements CarteFeraille, CarteAlliee {

    public Ram(){

        this.nom = "Ram";
        this.faction = Faction.BLOB;
        this.idImage = R.drawable.ram;
        this.cout = 3;
        this.abiliteeUtilise = false;
        this.abiliteeAllieUtilise = false;
    }

    @Override
    public void alliee(Joueur j) {
        j.ajouteDegat(2);
    }

    @Override
    public void ferailler(Joueur j) {
        j.ajouteOr(3);
    }

    @Override
    public void jouerCarte(Joueur j) {
            j.ajouteDegat(5);
    }

    @Override
    public int ajouteOrFeraille(){
        return 3;
    }
}
