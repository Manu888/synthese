package com.example.manuel.starrealms.Cards.Vaisseaux.Blob;

import com.example.manuel.starrealms.Cards.Modele.Vaisseau;
import com.example.manuel.starrealms.Cards.Modele.CarteAlliee;
import com.example.manuel.starrealms.Entity.Faction;
import com.example.manuel.starrealms.Entity.Joueur;
import com.example.manuel.starrealms.R;


public class Mothership extends Vaisseau implements CarteAlliee {

    public Mothership(){

        this.nom = "Mothership";
        this.faction = Faction.BLOB;
        this.idImage = R.drawable.mothership;
        this.cout = 7;
        this.abiliteeUtilise = false;
        this.abiliteeAllieUtilise = false;
    }
    @Override
    public void alliee(Joueur j) {
            j.pigerCarte(1);
    }

    @Override
    public void jouerCarte(Joueur j) {
            j.pigerCarte(1);
            j.ajouteDegat(6);
    }
}
