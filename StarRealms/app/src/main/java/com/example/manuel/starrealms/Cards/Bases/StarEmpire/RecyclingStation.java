package com.example.manuel.starrealms.Cards.Bases.StarEmpire;

import com.example.manuel.starrealms.Cards.Modele.AvantPoste;
import com.example.manuel.starrealms.Entity.Choix;
import com.example.manuel.starrealms.Entity.Faction;
import com.example.manuel.starrealms.Entity.IA;
import com.example.manuel.starrealms.Entity.Joueur;
import com.example.manuel.starrealms.R;


public class RecyclingStation extends AvantPoste {

    public RecyclingStation(){

        this.nom = "RecyclingStation";
        this.faction = Faction.STAR_EMPIRE;
        this.idImage = R.drawable.recyclingstation;
        this.cout = 4;
        this.vie = 4;
        this.abiliteeUtilise = false;
    }

    @Override
    public void jouerCarte(Joueur j) {
        if(j instanceof IA){
            Choix choix1 = new Choix(1,"Ajouter 1 or");
            Choix choix2 = new Choix(2,"Defausser jusqu'à 2 cartes pour en piger autant");

            ((IA)j).faireChoix(this, choix1,choix2);
        }
        else {
            j.faireChoixOrDefausse();
        }
    }
    @Override
    public void choixFait(int choix, Joueur j){
        if(choix == 1){
            j.ajouteOr(1);
        }
        else{
            ((IA)j).defausseEtPige(2);
        }
    }
}
