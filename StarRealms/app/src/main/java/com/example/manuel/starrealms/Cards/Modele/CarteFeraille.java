package com.example.manuel.starrealms.Cards.Modele;
import com.example.manuel.starrealms.Entity.Joueur;

// CLASSE QUI PERMET D'AJOUTER LA METHODE FÉRAILLER AUX CARTES L'IMPLÉMENTANT
public interface CarteFeraille {
    void ferailler(Joueur j);
}
