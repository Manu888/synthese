package com.example.manuel.starrealms.Cards.Vaisseaux.MachineCult;

import com.example.manuel.starrealms.Cards.Modele.Base;
import com.example.manuel.starrealms.Cards.Modele.Vaisseau;
import com.example.manuel.starrealms.Cards.Modele.CarteAlliee;
import com.example.manuel.starrealms.Entity.Faction;
import com.example.manuel.starrealms.Entity.IA;
import com.example.manuel.starrealms.Entity.Joueur;
import com.example.manuel.starrealms.R;


public class MissileMech extends Vaisseau implements CarteAlliee {

    public MissileMech(){

        this.nom = "MissileMech";
        this.faction = Faction.MACHINE_CULT;
        this.idImage = R.drawable.missilemech;
        this.cout = 6;
        this.abiliteeUtilise = false;
        this.abiliteeAllieUtilise = false;
    }

    @Override
    public void alliee(Joueur j) {
            j.pigerCarte(1);
    }

    @Override
    public void jouerCarte(Joueur j) {
        j.ajouteDegat(6);
        if(j instanceof IA){
            ((IA)j).choisirBaseADetruire();
        }
        else{
            j.faireChoixDetruireBase();
        }
    }

}
