package com.example.manuel.starrealms.Cards.Vaisseaux.TradeFederation;

import com.example.manuel.starrealms.Cards.Modele.Vaisseau;
import com.example.manuel.starrealms.Cards.Modele.CarteAlliee;
import com.example.manuel.starrealms.Entity.Faction;
import com.example.manuel.starrealms.Entity.IA;
import com.example.manuel.starrealms.Entity.Joueur;
import com.example.manuel.starrealms.R;


public class CommandShip extends Vaisseau implements CarteAlliee {

    public CommandShip(){

        this.nom = "CommandShip";
        this.faction = Faction.TRADE_FEDERATION;
        this.idImage = R.drawable.commandship;
        this.cout = 8;
        this.abiliteeUtilise = false;
        this.abiliteeAllieUtilise = false;
    }

    @Override
    public void alliee(Joueur j){
        if(j instanceof IA){
            ((IA)j).choisirBaseADetruire();
        }
        else{
            j.faireChoixDetruireBase();
        }

    }

    @Override
    public void jouerCarte(Joueur j) {
        j.ajouteDegat(5);
        j.ajouteVie(4);
        j.pigerCarte(2);
    }
}
