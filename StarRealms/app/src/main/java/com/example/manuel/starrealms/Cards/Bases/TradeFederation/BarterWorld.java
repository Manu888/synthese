package com.example.manuel.starrealms.Cards.Bases.TradeFederation;

import com.example.manuel.starrealms.Cards.Modele.Base;
import com.example.manuel.starrealms.Cards.Modele.CarteFeraille;
import com.example.manuel.starrealms.Entity.Choix;
import com.example.manuel.starrealms.Entity.Faction;
import com.example.manuel.starrealms.Entity.IA;
import com.example.manuel.starrealms.Entity.Joueur;
import com.example.manuel.starrealms.R;


public class BarterWorld extends Base implements CarteFeraille {

    public BarterWorld(){

        this.nom = "BarterWorld";
        this.faction = Faction.TRADE_FEDERATION;
        this.idImage = R.drawable.barterworld;
        this.cout = 4;
        this.vie = 4;
        this.abiliteeUtilise = false;
    }

    @Override
    public void ferailler(Joueur j) {
            j.ajouteDegat(5);
    }

    @Override
    public void jouerCarte(Joueur j) {
        if(j instanceof IA){
            Choix choix1 = new Choix(1,"Ajouter 2 vies");
            Choix choix2 = new Choix(2,"Ajouter 2 or");

            ((IA)j).faireChoix(this,choix1,choix2);
        }
        else{
            j.faireChoixOrVie();
        }
    }

    @Override
    public int ajouteCombatFeraille(){
        return 5;
    }

    @Override
    public void choixFait(int choix, Joueur j){
        if(choix == 1){
            j.ajouteVie(2);
        }
        else{
            j.ajouteOr(2);
        }
    }
}
