package com.example.manuel.starrealms.Cards.Vaisseaux.TradeFederation;

import com.example.manuel.starrealms.Cards.Modele.Vaisseau;
import com.example.manuel.starrealms.Cards.Modele.CarteAlliee;
import com.example.manuel.starrealms.Entity.Faction;
import com.example.manuel.starrealms.Entity.Joueur;
import com.example.manuel.starrealms.R;


public class Freighter extends Vaisseau implements CarteAlliee {

    public Freighter(){

        this.nom = "Freighter";
        this.faction = Faction.TRADE_FEDERATION;
        this.idImage = R.drawable.freighter;
        this.cout = 4;
        this.abiliteeUtilise = false;
        this.abiliteeAllieUtilise = false;
    }

    @Override
    public void alliee(Joueur j) {
        j.setProchainAchatSurDeck(true);
    }

    @Override
    public void jouerCarte(Joueur j) {
        j.ajouteOr(4);
    }
}
