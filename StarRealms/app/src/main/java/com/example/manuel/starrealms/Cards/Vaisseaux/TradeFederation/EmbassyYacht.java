package com.example.manuel.starrealms.Cards.Vaisseaux.TradeFederation;

import com.example.manuel.starrealms.Cards.Modele.Vaisseau;
import com.example.manuel.starrealms.Entity.Faction;
import com.example.manuel.starrealms.Entity.Joueur;
import com.example.manuel.starrealms.R;


public class EmbassyYacht extends Vaisseau {

    public EmbassyYacht(){

        this.nom = "EmbassyYacht";
        this.faction = Faction.TRADE_FEDERATION;
        this.idImage = R.drawable.embassyacht;
        this.cout = 3;
        this.abiliteeUtilise = false;
        this.abiliteeAllieUtilise = false;
    }

    @Override
    public void jouerCarte(Joueur j) {
        j.ajouteVie(3);
        j.ajouteOr(2);
        if(j.getBase().size()>= 2)
            j.pigerCarte(2);
    }
}
