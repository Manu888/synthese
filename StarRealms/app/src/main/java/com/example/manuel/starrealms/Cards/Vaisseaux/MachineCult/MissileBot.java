package com.example.manuel.starrealms.Cards.Vaisseaux.MachineCult;

import com.example.manuel.starrealms.Cards.Modele.Carte;
import com.example.manuel.starrealms.Cards.Modele.Vaisseau;
import com.example.manuel.starrealms.Cards.Modele.CarteAlliee;
import com.example.manuel.starrealms.Entity.Faction;
import com.example.manuel.starrealms.Entity.IA;
import com.example.manuel.starrealms.Entity.Joueur;
import com.example.manuel.starrealms.R;


public class MissileBot extends Vaisseau implements CarteAlliee {

    public MissileBot(){

        this.nom = "MissileBot";
        this.faction = Faction.MACHINE_CULT;
        this.idImage = R.drawable.missilebot;
        this.cout = 2;
        this.abiliteeUtilise = false;
        this.abiliteeAllieUtilise = false;
    }

    @Override
    public void alliee(Joueur j) {
        j.ajouteDegat(2);
    }

    @Override
    public void jouerCarte(Joueur j) {
        if(j instanceof IA){
            ((IA)j).feraillerCarteDeMainOuDefausse(1);
        }
        else{
            j.faireChoixFerraillerMainDefausse();
        }

    }


}
