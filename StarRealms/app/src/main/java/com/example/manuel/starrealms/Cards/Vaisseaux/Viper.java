package com.example.manuel.starrealms.Cards.Vaisseaux;

import com.example.manuel.starrealms.Cards.Modele.Vaisseau;
import com.example.manuel.starrealms.Entity.Faction;
import com.example.manuel.starrealms.Entity.Joueur;
import com.example.manuel.starrealms.R;


public class Viper extends Vaisseau {

    public Viper(){
        this.nom="Viper";
        this.faction = Faction.AUCUNE;
        this.idImage = R.drawable.viper;
        this.cout = 0 ;
        this.abiliteeUtilise = false;
        this.abiliteeAllieUtilise = false;
    }
    @Override
    public void jouerCarte(Joueur j) {
        j.ajouteDegat(1);
    }
}
