package com.example.manuel.starrealms.Interface;

import android.content.Intent;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.manuel.starrealms.Cards.Modele.Carte;
import com.example.manuel.starrealms.Entity.Joueur;
import com.example.manuel.starrealms.R;

import java.util.ArrayList;

import static com.example.manuel.starrealms.Interface.GameActivity.joueur;

public class EnJeuActivity extends AppCompatActivity {

    private ViewPager pager;
    private SwipeAdapter adapter;
    private Bundle b;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_en_jeu);

        pager = (ViewPager)findViewById(R.id.pager);

        // On récupere le bundle d'extras
        Intent i = getIntent();
        b = i.getExtras();

        // On associe le swipeAdapter au pager de l'activité
        adapter = new SwipeAdapter(getSupportFragmentManager());
        pager.setAdapter(adapter);

    }

    public class SwipeAdapter extends FragmentStatePagerAdapter{

        public SwipeAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // on créer une liste de cartes et on parcourt le bundle d'extra
            // pour savoir lesquelles sont des cartes et le joueur correspondant
            ArrayList<Carte>liste = new ArrayList<>();
            if (b != null) {
                for (String key : b.keySet()) {
                    // On travers les clé et on vérifie si l'objet récupèré
                    // est un joueur ou une carte
                    Object value = b.get(key);
                    if(value instanceof Joueur){
                        joueur = (Joueur)value;
                    }
                    else{
                        liste.add((Carte)value);
                    }
                }
            }
            // On retourne le fragment avec la carte à afficher
            return EnJeuFragment.newInstance(liste.get(position));
        }

        @Override
        public int getCount() {
            // Il y autant de fragment que de cartes dans le bundle
            return b.size();
        }
    }
}
