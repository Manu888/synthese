package com.example.manuel.starrealms.Cards.Bases.MachineCult;

import com.example.manuel.starrealms.Cards.Modele.AvantPoste;
import com.example.manuel.starrealms.Entity.Faction;
import com.example.manuel.starrealms.Entity.IA;
import com.example.manuel.starrealms.Entity.Joueur;
import com.example.manuel.starrealms.R;


public class BrainWorld extends AvantPoste {

    public BrainWorld(){

        this.nom = "BrainWorld";
        this.faction = Faction.MACHINE_CULT;
        this.idImage = R.drawable.brainworld;
        this.cout = 8;
        this.vie = 6;
        this.abiliteeUtilise = false;
    }

    @Override
    public void jouerCarte(Joueur j) {
        if(j instanceof IA){

            ((IA)j).choisirFeraillerPiger(2);
        }
        else{
            j.faireChoixFerraillePiger();
        }
    }
}
