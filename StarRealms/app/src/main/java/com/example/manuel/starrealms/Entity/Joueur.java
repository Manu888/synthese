package com.example.manuel.starrealms.Entity;


import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;

import com.example.manuel.starrealms.Cards.Modele.AvantPoste;
import com.example.manuel.starrealms.Cards.Modele.Base;
import com.example.manuel.starrealms.Cards.Bases.MachineCult.MechWorld;
import com.example.manuel.starrealms.Cards.Bases.StarEmpire.FleetHQ;
import com.example.manuel.starrealms.Cards.Modele.Carte;
import com.example.manuel.starrealms.Cards.Modele.CarteFeraille;
import com.example.manuel.starrealms.Cards.Modele.Vaisseau;
import com.example.manuel.starrealms.Cards.Vaisseaux.Explorer;
import com.example.manuel.starrealms.Cards.Vaisseaux.MachineCult.StealthNeedle;
import com.example.manuel.starrealms.Interface.GameActivity;
import com.example.manuel.starrealms.R;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

public class Joueur  {

    private String nom;
    private int vie = 50;
    private int or = 0;
    private int degat = 0;
    private int nbBrasse = 0;
    private int tour = 0;
    private boolean debute = false;
    private Carte carteSelectionnee = null;
    private List<Carte> main = new ArrayList<>();
    private List<Carte> deck = new ArrayList<>();
    private List<Carte> defausse= new ArrayList<>();
    private List<Carte> enJeu= new ArrayList<>();
    private List<Base> base= new ArrayList<>();
    private Set<Faction> factionsJouerCeTour = new HashSet<>();
    private Joueur adversaire = null;
    private Jeu jeu;
    private boolean allieeBlob = false;
    private boolean allieeStarEmpire = false;
    private boolean allieeMachineCult = false;
    private boolean allieeTradeFederation = false;
    private boolean tousFactionAlliee = false;
    private boolean ajouterDegatChqVaisseau = false;
    private boolean prochainVaisseauSurDeck = false;
    private boolean prochaineBaseDansMain = false;
    private boolean prochainAchatSurDeck = false;
    private boolean prochainAchatDansMain = false;
    private GameActivity Activity;

    private AlertDialog.Builder builderSingle;
    private AlertDialog.Builder builderSingle2;
    private ArrayAdapter<String> arrayAdapter;
    private ArrayAdapter<String> arrayAdapter2;

    // Fonction lambda permettant de comparer la vie des bases
    protected Comparator<Base> vieBase = (Base1,Base2) -> Integer.compare(Base1.getVie(),Base2.getVie());


    public Joueur(GameActivity act){
        // Quand on créer un joueur, on initialise une boite de dialog de choix
        this.setActivity(act);
        this.builderSingle = new AlertDialog.Builder(act);
        this.builderSingle.setIcon(R.drawable.icon);
        this.builderSingle2 = new AlertDialog.Builder(act);
        this.builderSingle2.setIcon(R.drawable.icon);

    }

    // ####################GETTERS ET SETTERS######################


    public AppCompatActivity getActivity() {
        return Activity;
    }

    public void setActivity(GameActivity activity) {
        Activity = activity;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getVie() {
        return vie;
    }

    public void setVie(int vie) {
        this.vie = vie;
    }

    public int getOr() {
        return or;
    }

    public void setOr(int or) {
        this.or = or;
    }

    public int getDegat() {
        return degat;
    }

    public void setDegat(int degat) {
        this.degat = degat;
    }

    public int getNbBrasse() {
        return nbBrasse;
    }

    public void setNbBrasse(int nbBrasse) {
        this.nbBrasse = nbBrasse;
    }

    public boolean isDebute() {
        return debute;
    }

    public void setDebute(boolean debute) {
        this.debute = debute;
    }

    public List<Carte> getMain() {
        return main;
    }

    public void setMain(List<Carte> main) {
        this.main = main;
    }

    public List<Carte> getDeck() {
        return deck;
    }

    public void setDeck(List<Carte> deck) {
        this.deck = deck;
    }

    public Carte getCarteSelectionnee() {
        return carteSelectionnee;
    }

    public void setCarteSelectionnee(Carte carteSelectionnee) {
        this.carteSelectionnee = carteSelectionnee;
    }

    public List<Carte> getDefausse() {
        return defausse;
    }

    public void setDefausse(List<Carte> defausse) {
        this.defausse = defausse;
    }

    public List<Carte> getEnJeu() {
        return enJeu;
    }

    public void setEnJeu(List<Carte> enJeu) {
        this.enJeu = enJeu;
    }

    public List<Base> getBase() {
        return base;
    }

    public List<AvantPoste> getAvantPoste() {
        return base.stream().filter(Carte::isAvantPoste).map(base -> (AvantPoste) base).collect(Collectors.toList());
    }

    public void setBase(List<Base> base) {
        this.base = base;
    }

    public Joueur getAdversaire() {
        return adversaire;
    }

    public void setAdversaire(Joueur adversaire) {
        this.adversaire = adversaire;
    }

    public boolean isAllieeBlob() {
        return allieeBlob;
    }

    public void setAllieeBlob(boolean allieeBlob) {
        this.allieeBlob = allieeBlob;
    }

    public boolean isAllieeStarEmpire() {
        return allieeStarEmpire;
    }

    public void setAllieeStarEmpire(boolean allieeStarEmpire) {
        this.allieeStarEmpire = allieeStarEmpire;
    }

    public boolean isAllieeMachineCult() {
        return allieeMachineCult;
    }

    public void setAllieeMachineCult(boolean allieeMachineCult) {
        this.allieeMachineCult = allieeMachineCult;
    }

    public boolean isAllieeTradeFederation() {
        return allieeTradeFederation;
    }

    public void setAllieeTradeFederation(boolean allieeTradeFederation) {
        this.allieeTradeFederation = allieeTradeFederation;
    }

    public boolean isAjouterDegatChqVaisseau() {
        return ajouterDegatChqVaisseau;
    }

    public void setAjouterDegatChqVaisseau(boolean ajouterDegatChqVaisseau) {
        this.ajouterDegatChqVaisseau = ajouterDegatChqVaisseau;
    }

    public boolean isProchainVaisseauSurDeck() {
        return prochainVaisseauSurDeck;
    }

    public void setProchainVaisseauSurDeck(boolean prochainVaisseauSurDeck) {
        this.prochainVaisseauSurDeck = prochainVaisseauSurDeck;
    }

    public int getTour() {
        return tour;
    }

    public void setTour(int tour) {
        this.tour = tour;
    }

    public boolean isProchaineBaseDansMain() {
        return prochaineBaseDansMain;
    }

    public void setProchaineBaseDansMain(boolean prochaineBaseDansMain) {
        this.prochaineBaseDansMain = prochaineBaseDansMain;
    }

    public boolean isProchainAchatSurDeck() {
        return prochainAchatSurDeck;
    }

    public void setProchainAchatSurDeck(boolean prochainAchatSurDeck) {
        this.prochainAchatSurDeck = prochainAchatSurDeck;
    }

    public boolean isProchainAchatDansMain() {
        return prochainAchatDansMain;
    }

    public void setProchainAchatDansMain(boolean prochainAchatDansMain) {
        this.prochainAchatDansMain = prochainAchatDansMain;
    }

    public boolean isTousFactionAlliee() {
        return tousFactionAlliee;
    }

    public void setTousFactionAlliee(boolean tousFactionAlliee) {
        this.tousFactionAlliee = tousFactionAlliee;
        factionsJouerCeTour.add(Faction.BLOB);
        factionsJouerCeTour.add(Faction.MACHINE_CULT);
        factionsJouerCeTour.add(Faction.STAR_EMPIRE);
        factionsJouerCeTour.add(Faction.TRADE_FEDERATION);
    }

    public Jeu getJeu() {
        return jeu;
    }

    public void setJeu(Jeu jeu) {
        this.jeu = jeu;
    }

    public Set<Faction> getFactionsJouerCeTour() {
        return factionsJouerCeTour;
    }

    public void setFactionsJouerCeTour(Set<Faction> factionsJouerCeTour) {
        this.factionsJouerCeTour = factionsJouerCeTour;
    }

    // ################################## MÉTHODES DU JOUEUR  ###########################################################

    // On ajoute à ce que le joueur possède deja
    public void ajouteVie(int nbVie){
        vie+=nbVie;
    }
    public void enleverVie(int nbVie){
        vie-= nbVie;
    }
    public void ajouteDegat(int nbDegat){
        degat += nbDegat;
    }
    public void ajouteOr(int nbOr){
        or+= nbOr;
    }
    public void ajouterCarteDansMain(Carte c){
        main.add(c);
    }
    public void ajouterBase(Base b){
        base.add(b);
    }
    public void ajouterBrasse(){
        nbBrasse+=1;
    }


    // Fonction qui defausse une carte voulue
    public void defausserCarte(Carte c){
        // Il semble y avoir un problème avec la fonction remove du ArrayList donc
        // je cherche manuellement l'index de la carte a defausser
        int index = 0;
        for(int i = 0; i> getMain().size();i++){
            if(getMain().get(i).getNom().equals(c.getNom())){
                index = i;
                break;
            }
        }
        main.remove(index);
        defausse.add(c);
        carteQuittantleJeu(c);
    }

    // Fonction qui fait defausser un IA
    public void adversaireDefausse(){
        List<Carte> aJeter= ((IA)adversaire).getCarteADefausser(1,false);

    }
    // Fonction qui permet de defausser pour piger autant
    public void defausserPourPiger(Carte c){
        defausserCarte(c);
        pigerCarte(1);
    }

    // Fonction qui permet de sortir sécuritairement une carte du jeu
    public void carteQuittantleJeu(Carte c){
        c.setAbiliteeUtilise(false);
        if (c instanceof MechWorld){
            tousFactionAlliee = false;
        }
        else if(c instanceof StealthNeedle){
            ((StealthNeedle)c).setCarteCopiee(null);
        }
        else if(c instanceof FleetHQ){
            ajouterDegatChqVaisseau = false;
        }
        if(c instanceof Base){
            ((Base) c).setUtilisee(false);

        }
    }
    // Méthode qui fait defausser une carte précise à un adversaire
    public void faitDefausserCarte(Carte c){
        adversaire.defausserCarte(c);
    }

    // Méthode qui permet de piger un nombre de cartes
    public void pigerCarte(int nbCarte){
        for(int i=0; i<nbCarte;i++){
            // Si le deck est vide, on doit prendre la defausse, la brasser afin de reformer un deck
            if(deck.size() == 0){
                deck.addAll(defausse);
                defausse.clear();
                Collections.shuffle(deck);
                nbBrasse++;
            }
            Carte nouvelleCarte = deck.remove(0);
            ajouterCarteDansMain(nouvelleCarte);

        }
    }

    // Fonction qui détruit une base chez le joueur actuel
    public void detruireBase(Base b){

        for(int i = 0; i<base.size();i++){
            if(b.getNom().equals(base.get(i).getNom())){
                this.base.remove(i);
            }
        }

        this.defausse.add(b);
        carteQuittantleJeu(b);
    }

    // Fonction qui détruit une base chez l'adversaire
    public void detruireBaseCible(Base b){
        adversaire.detruireBase(b);
    }

    // Fonction qui déduit les points de dégats correspondant à l'attaque d'une base
    public void detruireBaseEnnemie(Base b){
        if(b.getVie()<= degat){
            degat -= b.getVie();
            adversaire.detruireBase(b);
        }
    }
    // Fonction qui permet de férailler pour piger
    public void ferrailleMainPourPiger(Carte c){
        main.remove(c);
        pigerCarte(1);

    }
    // Fonction qui permet de férailler de la défausse pour piger
    public void ferrailleDefaussePourPiger(Carte c){
        defausse.remove(c);
        pigerCarte(1);

    }
    // Fonction qui permet de férailler une carte de la main
    public void ferailleDeMain(Carte c){
        main.remove(c);
        feraillerCarte(c);
    }
    // Fonction qui permet de férailler de la défausse
    public void ferailleDeDefausse(Carte c){
        defausse.remove(c);
        feraillerCarte(c);
    }

    public void feraillerCarte(Carte c){
        if(c.isFeraille()) {
            int index = 0;
            // On doit chercher manuellement la carte car la méthode EnJeu.contains() ne
            // prend en compte que les objets de class Carte (pas les Avant-poste et les Bases)
            for(int i = 0; i< enJeu.size();i++){
                if(enJeu.get(i).getNom().equals(c.getNom())){
                    index = i;
                }
            }

            Carte card = enJeu.get(index);
            enJeu.remove(card);

            if(c.isBase()){
                base.remove(card);
            }
            carteQuittantleJeu(c);
            ((CarteFeraille)c).ferailler(this);

        }
    }

    public void ferraillerCarteduMarche(Carte c){
        jeu.ferrailleCarteDuMarche(c);
    }


    public void ajouterCarteSurDeck(Carte c){
        deck.add(0,c);
        
    }
    public List<Base> getBaseInutilisee(){
        List<Base> inutilisee = new ArrayList<>();
         for(Base b : base){
             if(!b.isAbiliteeUtilise()){
                 inutilisee.add(b);
             }

         }
        return inutilisee;
    }

    public int getBasePlusVie(){
        int vie = 0;

        if(!base.isEmpty()){
            List<Base> basesTrie = base.stream().sorted(vieBase).collect(toList());
            vie = basesTrie.get(0).getVie();
        }

        return vie;
    }
    public int getBaseMoinsVie(){
        int vie = 0;

        if(!base.isEmpty()){
            List<Base> basesTrie = base.stream().sorted(vieBase.reversed()).collect(toList());
            vie = basesTrie.get(0).getVie();
        }

        return vie;
    }

    public int getAvantPosteMoinsVie() {
        if (!getAvantPoste().isEmpty()) {
            List<AvantPoste> avantPostesClasse = getAvantPoste().stream().sorted(vieBase).collect(toList());
            return avantPostesClasse.get(0).getVie();
        }
        return 0;
    }
    public void copieVaisseau(StealthNeedle vaisseauAcopier){
        if(!enJeu.isEmpty()){
            for(Carte c :enJeu){
                if(c instanceof StealthNeedle){
                    ((StealthNeedle) c).setCarteCopiee(vaisseauAcopier);
                }
            }
        }
    }
    public void acheterCarte(Carte c){
        if(or>=c.getCout()){
            or -= c.getCout();
            acquerirCarte(c);
            if(!c.getNom().equals("Explorer")){
                jeu.getMarche().remove(c);
                jeu.ajouteNombreCarteAuMarche(1);
            }
        }
    }
    // Fonction qui permet d'obtenir une carte de la placer à l'endroit approprié
    public void acquerirCarte(Carte c){
        if(c.isVaisseau() && prochainVaisseauSurDeck || prochainAchatSurDeck){
            prochainVaisseauSurDeck = false;
            prochainAchatSurDeck = false;
            ajouterCarteSurDeck(c);
        }
        else if(c.isBase() && prochaineBaseDansMain || prochainAchatDansMain ){
            prochaineBaseDansMain = false;
            prochainAchatDansMain = false;
            ajouterCarteDansMain(c);
        }
        else if(c.isBase() && prochainAchatSurDeck){
            prochainAchatSurDeck = false;
            ajouterCarteSurDeck(c);
        }
        else{
            defausse.add(c);
        }
    }


    public void acquerirCarteGratuitement(Carte c){
        acquerirCarte(c);
        jeu.getMarche().remove(c);
        jeu.ajouteNombreCarteAuMarche(1);
    }

    // Fonction qui permet d'attaquer l'adversaire avec tous les points restants
    public void attaquerAdversaire(){
        adversaire.enleverVie(degat);
        degat=0;
    }

    // Fonction qui renvoie toutes les cartes de toutes les endroits possible chez le joueur
    public List<Carte> toutesLesCartes(){
        List<Carte> sommaire = new ArrayList<>();
        sommaire.addAll(main);
        sommaire.addAll(defausse);
        sommaire.addAll(deck);
        if(!enJeu.isEmpty()){
            sommaire.addAll(enJeu);
        }
        if(!base.isEmpty()){
            sommaire.addAll(base);
        }
        return sommaire;
    }
    // Fonction qui permet de retourner le nombre de carte du type passée en paramètre
    public int compteCarteParType(List<Carte> cartes, Function<Carte, Boolean> typeMatcher) {
        return (int) cartes.stream().filter(typeMatcher::apply).count();
    }
    // Fonction de compte divers
    public int compteVaisseau(){
        return compteCarteParType(toutesLesCartes(), Carte::isVaisseau);
    }
    public int compteBase(){
        return compteCarteParType(toutesLesCartes(), Carte::isBase);
    }
    public int compteAvantPoste(){return compteCarteParType(toutesLesCartes(), Carte::isAvantPoste);
    }

    // Retourne la faction la plus nombreuse
    public Faction factionLaPlusNombreuse(){
        // on prend toutes les cartes et on le compare avec le Hashset de toutes les factions
        List<Carte> cartes = toutesLesCartes();
        Map<Faction,Integer> compteFaction = new HashMap<>(4);

        // Emploi de la méthode de compte par type défini plus haut
        compteFaction.put(Faction.MACHINE_CULT,compteCarteParType(cartes,Carte::isMachineCult));
        compteFaction.put(Faction.BLOB,compteCarteParType(cartes,Carte::isBlob));
        compteFaction.put(Faction.STAR_EMPIRE,compteCarteParType(cartes,Carte::isStarEmpire));
        compteFaction.put(Faction.TRADE_FEDERATION,compteCarteParType(cartes,Carte::isTradeFederation));

        Faction factionPopulaire = null;
        int plusNombreuse = 0;

        for(Faction f: compteFaction.keySet()){
            if(compteFaction.get(f)>= plusNombreuse)
                factionPopulaire = f;
        }

        return factionPopulaire;
    }

    // Fonction qui permet de savoir si on peut finir une base avec les dégats
    public boolean peutDetruireBaseAvecDegat(int extraCombat) {
        if (getAdversaire().getBase().size() > 0) {
            if (getAdversaire().getAvantPoste().size() > 0) {
                if (getDegat() < getAdversaire().getAvantPosteMoinsVie()) {
                    return (getDegat() + extraCombat) >= getAdversaire().getAvantPosteMoinsVie();
                }
            } else if (getAdversaire().getBase().size() > 0) {
                if (getDegat() < getAdversaire().getBaseMoinsVie()) {
                    return (getDegat() + extraCombat) >= getAdversaire().getBaseMoinsVie();
                }
            }
        }
        return false;
    }

    // Permet de savoir si la faction a déja été jouer ce tour
    public boolean dejaJouerCarteFaction(Carte c){
        boolean dejaJouer = false;
        if(tousFactionAlliee)
            dejaJouer=true;
        if(c.isBlob() && allieeBlob )
            dejaJouer=true;
        if(c.isStarEmpire() && allieeStarEmpire )
            dejaJouer=true;
        if(c.isMachineCult() && allieeMachineCult )
            dejaJouer=true;
        if(c.isTradeFederation() && allieeTradeFederation )
            dejaJouer=true;

        for (Carte check: enJeu){
            if(check != c && check.isAlliee(c))
                dejaJouer=true;
        }

        return dejaJouer;

    }
    // Fonction qui permet de jouer une carte par le joueur
    public void jouerCarte(Carte c){
        enJeu.add(c);

        // On doit rechercher si le nom est actuellement dans la liste de base au lieu d'utiliser
        // la méthode Arraylist.contains() car la array ne comparera que les bases et non les avants-poste (qui sont aussi des Base...)

        boolean baseEnMain = false;
        if(c instanceof Base){

            for(Carte car: main){
                if(car.getNom().equals(c.getNom())){
                    baseEnMain = true;
                }
            }
        }

        // Si la base n'est pas en main c'est qu'on a pas besoin de l'ajouter a la liste des bases
        // car elle est deja dans la base ( il ne faut pas oublier qu'on peut avoir plusieurs fois la même base)
        if(c.isBase() && baseEnMain)
            ajouterBase((Base)c);

        if(c.isVaisseau())
            if(ajouterDegatChqVaisseau)
                ajouteDegat(1);

        factionsJouerCeTour.add(c.getFaction());
        main.remove(c);
        c.setAbiliteeUtilise(true);
        c.jouerCarte(this);

    }

    public void retourneBaseDansMain(Base b){
        base.remove(b);
        main.add(b);
        carteQuittantleJeu(b);
    }

    // Permet de savoir si la faction est jouée ce tour-ci
    public boolean factionJouerCeTour(Faction faction) {
        return factionsJouerCeTour.contains(faction);
    }

    // Fonction qui permet de réinitialiser les informations d'un joueur
    public void finirTour(){
        tour++;
        degat=0;
        or=0;

        // On réinitialise tous les bool à faux
        prochainVaisseauSurDeck = false;
        prochaineBaseDansMain = false;
        prochainAchatSurDeck = false;
        prochainAchatDansMain = false;
        allieeBlob= false;
        allieeStarEmpire= false;
        allieeMachineCult= false;
        allieeTradeFederation= false;

        for (Carte c : enJeu){
            c.setAbiliteeUtilise(false);
            c.setAbiliteeAllieUtilise(false);
            if(c.isBase()){
                ((Base) c).setUtilisee(false);

            }
            else{
                defausse.add(c);
                carteQuittantleJeu(c);
            }
        }
        enJeu.clear();
        defausse.addAll(main);
        main.clear();
        factionsJouerCeTour.clear();
        pigerCarte(5);
    }
    // Fonction qui permet d'afficher un choix de vaisseau à copier par le StealthNeedle
    public void faireChoixCopieVaisseau(StealthNeedle sn){
        // On ajoute toutes les vaisseaux déja joués ce tour sauf lui-même
        this.builderSingle.setTitle("Choissisez la carte a copier avec le StealthNeedle : ");
        this.arrayAdapter= new ArrayAdapter<String>(this.Activity, android.R.layout.select_dialog_singlechoice);
        for(Carte c:this.enJeu){
            if(!c.getNom().equals(sn.getNom())){
                this.arrayAdapter.add(c.getNom());
            }
        }
        builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Lorsque l'usager fait son choix, on récupere son choix
                String nomCible = arrayAdapter.getItem(which);
                // On créer une fenêtre de confirmation du choix
                AlertDialog.Builder builderInner = new AlertDialog.Builder(Activity);
                builderInner.setMessage(nomCible);
                Carte c = Joueur.this.getEnJeu().get(which);
                // On joue le vaisseau maintenant qu'il est une copie d'un vaisseau
                builderInner.setTitle("Vous avez choisie la carte suivante : ");
                c.setAbiliteeUtilise(false);
                sn.setCarteCopiee(c);
                Joueur.this.jouerCarte(sn.getCarteCopiee());
                Activity.raffraichirAffichage();
                // On met un bouton de confirmation qui permet de fermer la boite de dialog
                builderInner.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog,int which) {
                        dialog.dismiss();
                    }
                });
                // On affiche la confirmation du choix
                builderInner.show();
            }
        });
        this.builderSingle.show();
    }

    public void faireChoixDefausse(IA ia){
        // On récupere les mains que le joueur possède pour savoir laquelle il veut défausser
        this.builderSingle.setTitle("Vous devez défausser une carte : ");
        this.arrayAdapter= new ArrayAdapter<String>(this.Activity, android.R.layout.select_dialog_singlechoice);
        for(Carte c:this.adversaire.main){
        this.arrayAdapter.add(c.getNom());
        }
        builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Lorsqu'il clique sur un choix, on récupère l'index du choix pour retrouver la carte
                String nomCible = arrayAdapter.getItem(which);
                // On créer une fenêtre de confirmation du choix
                AlertDialog.Builder builderInner = new AlertDialog.Builder(Activity);
                builderInner.setMessage(nomCible);
                Carte c = Joueur.this.getMain().get(which);
                builderInner.setTitle("Vous avez défausser la carte suivante : ");
                // l'adversaire se défausse de la carte puis on raffraichit l'affichage
                adversaire.defausserCarte(c);
                Activity.raffraichirAffichage();
                builderInner.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    // Lorsque le joueur confirme, on ferme la fenêtre
                    public void onClick(DialogInterface dialog,int which) {
                        dialog.dismiss();
                    }
                });
                builderInner.show();
            }
        });
        this.builderSingle.show();
    }
    public void faireChoixDetruireBase(){
        // On rempli la sélection des bases à détruire avec les bases que l'adversaire possède
        this.builderSingle.setTitle("Choissisez la base a détruire : ");
        this.arrayAdapter= new ArrayAdapter<String>(this.Activity, android.R.layout.select_dialog_singlechoice);
        for(Carte c:this.adversaire.getBase()){
            this.arrayAdapter.add(c.getNom());
        }
        builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String nomCible = arrayAdapter.getItem(which);
                AlertDialog.Builder builderInner = new AlertDialog.Builder(Activity);
                builderInner.setMessage(nomCible);
                Base b = Joueur.this.adversaire.getBase().get(which);
                builderInner.setTitle("Vous avez détruit la base suivante : ");
                Joueur.this.adversaire.detruireBase(b);
                Activity.raffraichirAffichage();
                builderInner.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog,int which) {
                        dialog.dismiss();
                    }
                });
                builderInner.show();
            }
        });
        this.builderSingle.show();
    }
    public void faireChoixFerailleMarche(){
        this.builderSingle.setTitle("Choissisez la carte à férailler du marché : ");
        this.arrayAdapter= new ArrayAdapter<String>(this.Activity, android.R.layout.select_dialog_singlechoice);
        for(Carte c:this.jeu.getMarche()){
            this.arrayAdapter.add(c.getNom());
        }
        builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String nomCible = arrayAdapter.getItem(which);
                AlertDialog.Builder builderInner = new AlertDialog.Builder(Activity);
                builderInner.setMessage(nomCible);
                Carte c = Joueur.this.jeu.getMarche().get(which);
                builderInner.setTitle("Vous avez féraillez la carte suivante : ");
                Joueur.this.jeu.ferrailleCarteDuMarche(c);
                Activity.raffraichirAffichage();
                builderInner.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog,int which) {
                        dialog.dismiss();
                    }
                });
                builderInner.show();
            }
        });
        this.builderSingle.show();
    }

    public void faireChoixAchatGratuit(){

        this.builderSingle.setTitle("Choissisez le vaisseau à acquérir gratuitement : ");
        this.arrayAdapter= new ArrayAdapter<String>(this.Activity, android.R.layout.select_dialog_singlechoice);
        for(Carte c:this.jeu.getMarche()){
            if(c instanceof Vaisseau)
                this.arrayAdapter.add(c.getNom());
        }
        this.builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String nomCible = arrayAdapter.getItem(which);
                AlertDialog.Builder builderInner = new AlertDialog.Builder(Activity);
                builderInner.setMessage(nomCible);
                Carte c = Joueur.this.jeu.getMarche().get(which);
                builderInner.setTitle("Vous avez acquérit le vaisseau suivant : ");
                prochainVaisseauSurDeck = true;
                Joueur.this.acquerirCarteGratuitement(c);
                Activity.raffraichirAffichage();
                builderInner.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog,int which) {
                        dialog.dismiss();
                    }
                });
                builderInner.show();
            }
        });
        this.builderSingle.show();

    }

    public void faireChoixFerraillerMain(){
        Joueur.this.builderSingle2.setTitle("Ferrailler une carte de votre main : ");
        Joueur.this.arrayAdapter2= new ArrayAdapter<String>(Activity, android.R.layout.select_dialog_singlechoice);
        for(Carte c: Joueur.this.getMain()){
            Joueur.this.arrayAdapter2.add(c.getNom());
        }
        Joueur.this.builderSingle2.setAdapter(arrayAdapter2, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {
                String cartechoisi = arrayAdapter2.getItem(which);
                AlertDialog.Builder builderConfirm = new AlertDialog.Builder(Activity);
                builderConfirm.setMessage(cartechoisi);
                Carte c = Joueur.this.getMain().get(which);
                builderConfirm.setTitle("Vous avez féraillez la carte suivante : ");
                Joueur.this.ferailleDeMain(c);
                Activity.raffraichirAffichage();
                builderConfirm.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog,int which) {
                        dialog.dismiss();
                    }
                });
                builderConfirm.show();
            }

        });
        Joueur.this.builderSingle2.show();
    }

    public void faireChoixFerraillerMainDefausse(){

        this.builderSingle.setTitle("Faites votre choix : ");
        this.arrayAdapter= new ArrayAdapter<String>(this.Activity, android.R.layout.select_dialog_singlechoice);
        this.arrayAdapter.add("1 - Ferrailler de votre main");
        this.arrayAdapter.add("2 - Ferrailler de votre défausse");
        this.builderSingle.setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String nomCible = arrayAdapter.getItem(which);
                if(which ==0){
                    Joueur.this.builderSingle2.setTitle("Ferrailler une carte de votre main : ");
                    Joueur.this.arrayAdapter2= new ArrayAdapter<String>(Activity, android.R.layout.select_dialog_singlechoice);
                    for(Carte c: Joueur.this.getMain()){
                        Joueur.this.arrayAdapter2.add(c.getNom());
                    }
                    Joueur.this.builderSingle2.setAdapter(arrayAdapter2, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int which) {
                            String cartechoisi = arrayAdapter2.getItem(which);
                            AlertDialog.Builder builderConfirm = new AlertDialog.Builder(Activity);
                            builderConfirm.setMessage(cartechoisi);
                            Carte c = Joueur.this.getMain().get(which);
                            builderConfirm.setTitle("Vous avez féraillez la carte suivante : ");
                            Joueur.this.ferailleDeMain(c);
                            Activity.raffraichirAffichage();
                            builderConfirm.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog,int which) {
                                    dialog.dismiss();
                                }
                            });
                            builderConfirm.show();
                        }

                    });
                    Joueur.this.builderSingle2.show();
                }
                else{
                    Joueur.this.builderSingle2.setTitle("Ferrailler une carte de votre défausse : ");
                    Joueur.this.arrayAdapter2= new ArrayAdapter<String>(Activity, android.R.layout.select_dialog_singlechoice);
                    for(Carte c: Joueur.this.getDefausse()){
                        Joueur.this.arrayAdapter2.add(c.getNom());
                    }
                    Joueur.this.builderSingle2.setAdapter(arrayAdapter2, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int which) {
                            String cartechoisi = arrayAdapter2.getItem(which);
                            AlertDialog.Builder builderConfirm = new AlertDialog.Builder(Activity);
                            builderConfirm.setMessage(cartechoisi);
                            Carte c = Joueur.this.getDefausse().get(which);
                            builderConfirm.setTitle("Vous avez féraillez la carte suivante : ");
                            Joueur.this.ferailleDeDefausse(c);
                            Activity.raffraichirAffichage();
                            builderConfirm.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog,int which) {
                                    dialog.dismiss();
                                }
                            });
                            builderConfirm.show();
                        }

                    });
                    Joueur.this.builderSingle2.show();

                }
            }
        });
        this.builderSingle.show();

    }
        public void faireChoixOrDegat(){
            this.builderSingle.setTitle("Faites votre choix : ");
            this.arrayAdapter= new ArrayAdapter<String>(this.Activity, android.R.layout.select_dialog_singlechoice);
            this.arrayAdapter.add("1- Ajouter 3 or");
            this.arrayAdapter.add("2- Ajouter 5 dégats");
            builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    String nomCible = arrayAdapter.getItem(which);
                    AlertDialog.Builder builderInner = new AlertDialog.Builder(Activity);
                    builderInner.setMessage(nomCible);
                    builderInner.setTitle("Vous avez décidez de : ");
                    if(which==0){
                        Joueur.this.ajouteOr(3);
                    }
                    else{
                        Joueur.this.ajouteDegat(5);
                    }
                    Activity.raffraichirAffichage();
                    builderInner.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog,int which) {
                            dialog.dismiss();
                        }
                    });
                    builderInner.show();
                }
            });
            this.builderSingle.show();
        }

        public void faireChoixFerraillePiger(){
            this.builderSingle.setTitle("Faites votre choix : ");
            this.arrayAdapter= new ArrayAdapter<String>(this.Activity, android.R.layout.select_dialog_singlechoice);
            this.arrayAdapter.add("1 - Ferrailler de votre main pour piger");
            this.arrayAdapter.add("2 - Ferrailler de votre défausse pour piger");
            this.builderSingle.setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    String nomCible = arrayAdapter.getItem(which);
                    if(which ==0){
                        Joueur.this.builderSingle2.setTitle("Ferrailler une ou deux cartes de votre main pour piger : ");
                        Joueur.this.arrayAdapter2= new ArrayAdapter<String>(Activity, android.R.layout.select_dialog_multichoice);
                        String[] nomCartes = new String[Joueur.this.getMain().size()];
                        for(int i = 0; i< Joueur.this.getMain().size();i++){
                            nomCartes[i]=Joueur.this.getMain().get(i).getNom();
                        }
                        boolean[] isSelected = new boolean[Joueur.this.getMain().size()];
                        ArrayList<Integer> choix = new ArrayList();
                        Joueur.this.builderSingle2.setMultiChoiceItems(nomCartes, isSelected, new DialogInterface.OnMultiChoiceClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int which, boolean isChecked) {
                                if(isChecked){
                                    choix.add(which);
                                }
                                else if(choix.contains(which)){
                                    choix.remove(Integer.valueOf(which));
                                }
                            }
                        });
                        Joueur.this.builderSingle2.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog,int which) {
                                for(int k=0; k<choix.size();k++){
                                    int index = choix.get(k);
                                    Joueur.this.ferrailleMainPourPiger(Joueur.this.getMain().get(index));
                                }
                                Activity.raffraichirAffichage();
                                dialog.dismiss();
                            }
                        });
                        Joueur.this.builderSingle2.show();
                    }
                    else{

                        Joueur.this.builderSingle2.setTitle("Ferrailler une ou deux cartes de votre défausse pour piger: ");
                        Joueur.this.arrayAdapter2= new ArrayAdapter<String>(Activity, android.R.layout.select_dialog_multichoice);
                        String[] nomCartes = new String[Joueur.this.getDefausse().size()];
                        for(int i = 0; i< Joueur.this.getDefausse().size();i++){
                            nomCartes[i]=Joueur.this.getDefausse().get(i).getNom();
                        }
                        boolean[] isSelected = new boolean[Joueur.this.getDefausse().size()];
                        ArrayList<Integer> choix = new ArrayList();
                        Joueur.this.builderSingle2.setMultiChoiceItems(nomCartes, isSelected, new DialogInterface.OnMultiChoiceClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int which, boolean isChecked) {
                                if(isChecked){
                                    choix.add(which);
                                }
                                else if(choix.contains(which)){
                                    choix.remove(Integer.valueOf(which));
                                }
                            }
                        });
                        Joueur.this.builderSingle2.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog,int which) {
                                for(int k=0; k<choix.size();k++){
                                    int index = choix.get(k);
                                    Joueur.this.ferrailleDefaussePourPiger(Joueur.this.getMain().get(index));
                                }
                                Activity.raffraichirAffichage();
                                dialog.dismiss();
                            }
                        });
                        Joueur.this.builderSingle2.show();
                    }
                }
            });
            this.builderSingle.show();

        }

        public void faireChoixDegatPigeBlob(){
            this.builderSingle.setTitle("Faites votre choix : ");
            this.arrayAdapter= new ArrayAdapter<String>(this.Activity, android.R.layout.select_dialog_singlechoice);
            this.arrayAdapter.add("1- Ajouter 5 dégats");
            this.arrayAdapter.add("2- Piger une carte pour chaque carte Blob jouée ce tour-ci");
            builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    String nomCible = arrayAdapter.getItem(which);
                    AlertDialog.Builder builderInner = new AlertDialog.Builder(Activity);
                    builderInner.setMessage(nomCible);
                    builderInner.setTitle("Vous avez décidez de : ");
                    if(which==0){
                        Joueur.this.ajouteDegat(5);
                    }
                    else{
                        List<Carte> enjeu = Joueur.this.getEnJeu();
                        int compteblob = Joueur.this.compteCarteParType(Joueur.this.getEnJeu(), Carte::isBlob);
                        Joueur.this.pigerCarte(compteblob);
                    }
                    Activity.raffraichirAffichage();
                    builderInner.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog,int which) {
                            dialog.dismiss();
                        }
                    });
                    builderInner.show();
                }
            });
            this.builderSingle.show();

        }

        public void faireChoixVieDegat(){
            this.builderSingle.setTitle("Faites votre choix : ");
            this.arrayAdapter= new ArrayAdapter<String>(this.Activity, android.R.layout.select_dialog_singlechoice);
            this.arrayAdapter.add("1- Ajouter 3 vies");
            this.arrayAdapter.add("2- Ajouter 2 dégats");
            builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    String nomCible = arrayAdapter.getItem(which);
                    AlertDialog.Builder builderInner = new AlertDialog.Builder(Activity);
                    builderInner.setMessage(nomCible);
                    builderInner.setTitle("Vous avez décidez de : ");
                    if(which==0){
                        Joueur.this.ajouteVie(3);
                    }
                    else{
                        Joueur.this.ajouteDegat(2);
                    }
                    Activity.raffraichirAffichage();
                    builderInner.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog,int which) {
                            dialog.dismiss();
                        }
                    });
                    builderInner.show();
                }
            });
            this.builderSingle.show();
        }

        public void faireChoixVieOr(){
            this.builderSingle.setTitle("Faites votre choix : ");
            this.arrayAdapter= new ArrayAdapter<String>(this.Activity, android.R.layout.select_dialog_singlechoice);
            this.arrayAdapter.add("1- Ajouter 1 vie");
            this.arrayAdapter.add("2- Ajouter 1 or");
            builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    String nomCible = arrayAdapter.getItem(which);
                    AlertDialog.Builder builderInner = new AlertDialog.Builder(Activity);
                    builderInner.setMessage(nomCible);
                    builderInner.setTitle("Vous avez décidez de : ");
                    if(which==0){
                        Joueur.this.ajouteVie(1);
                    }
                    else{
                        Joueur.this.ajouteOr(1);
                    }
                    Activity.raffraichirAffichage();
                    builderInner.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog,int which) {
                            dialog.dismiss();
                        }
                    });
                    builderInner.show();
                }
            });
            this.builderSingle.show();

        }

        public void faireChoixOrVie(){
            this.builderSingle.setTitle("Faites votre choix : ");
            this.arrayAdapter= new ArrayAdapter<String>(this.Activity, android.R.layout.select_dialog_singlechoice);
            this.arrayAdapter.add("1- Ajouter 2 vies");
            this.arrayAdapter.add("2- Ajouter 2 or");
            builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    String nomCible = arrayAdapter.getItem(which);
                    AlertDialog.Builder builderInner = new AlertDialog.Builder(Activity);
                    builderInner.setMessage(nomCible);
                    builderInner.setTitle("Vous avez décidez de : ");
                    if(which==0){
                        Joueur.this.ajouteVie(2);
                    }
                    else{
                        Joueur.this.ajouteOr(2);
                    }
                    Activity.raffraichirAffichage();
                    builderInner.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog,int which) {
                            dialog.dismiss();
                        }
                    });
                    builderInner.show();
                }
            });
            this.builderSingle.show();
        }

        public void faireChoixOrDefausse(){
            this.builderSingle.setTitle("Faites votre choix : ");
            this.arrayAdapter= new ArrayAdapter<String>(this.Activity, android.R.layout.select_dialog_singlechoice);
            this.arrayAdapter.add("1 - Ajouter 1 or");
            this.arrayAdapter.add("2 - Défausser pour piger");
            this.builderSingle.setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    String nomCible = arrayAdapter.getItem(which);
                    if(which ==0){
                        AlertDialog.Builder builderInner = new AlertDialog.Builder(Activity);
                        builderInner.setMessage(nomCible);
                        builderInner.setTitle("Vous avez décidez de : ");
                        Joueur.this.ajouteOr(1);
                        Activity.raffraichirAffichage();
                        builderInner.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog,int which) {
                                dialog.dismiss();
                            }
                        });
                        builderInner.show();
                    }
                    else{

                        Joueur.this.builderSingle2.setTitle("Defaussez vous de deux cartes pour piger : ");
                        Joueur.this.arrayAdapter2= new ArrayAdapter<String>(Activity, android.R.layout.select_dialog_multichoice);
                        String[] nomCartes = new String[Joueur.this.getMain().size()];
                        for(int i = 0; i< Joueur.this.getMain().size();i++){
                            nomCartes[i]=Joueur.this.getMain().get(i).getNom();
                        }
                        boolean[] isSelected = new boolean[Joueur.this.getMain().size()];
                        ArrayList<Integer> choix = new ArrayList();
                        Joueur.this.builderSingle2.setMultiChoiceItems(nomCartes, isSelected, new DialogInterface.OnMultiChoiceClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int which, boolean isChecked) {
                                if(isChecked){
                                    choix.add(which);
                                }
                                else if(choix.contains(which)){
                                    choix.remove(Integer.valueOf(which));
                                }
                            }
                        });
                        Joueur.this.builderSingle2.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog,int which) {
                                for(int k=0; k<choix.size();k++){
                                    int index = choix.get(k);
                                    Joueur.this.defausserPourPiger(Joueur.this.getMain().get(index));
                                }
                                Activity.raffraichirAffichage();
                                dialog.dismiss();
                            }
                        });
                        Joueur.this.builderSingle2.show();
                    }
                }
            });
            this.builderSingle.show();

        }
    }

