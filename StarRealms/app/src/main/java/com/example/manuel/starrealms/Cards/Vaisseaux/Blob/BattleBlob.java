package com.example.manuel.starrealms.Cards.Vaisseaux.Blob;

import com.example.manuel.starrealms.Cards.Modele.CarteFeraille;
import com.example.manuel.starrealms.Cards.Modele.Vaisseau;
import com.example.manuel.starrealms.Cards.Modele.CarteAlliee;
import com.example.manuel.starrealms.Entity.Faction;
import com.example.manuel.starrealms.Entity.Joueur;
import com.example.manuel.starrealms.R;


public class BattleBlob extends Vaisseau implements CarteAlliee, CarteFeraille {

    public BattleBlob(){

        this.nom = "BattleBlob";
        this.faction = Faction.BLOB;
        this.idImage = R.drawable.battleblob;
        this.cout = 6;
        this.abiliteeUtilise = false;
        this.abiliteeAllieUtilise = false;
    }

    @Override
    public void alliee(Joueur j) {
        j.pigerCarte(1);
    }

    @Override
    public void ferailler(Joueur j) {
        j.ajouteDegat(4);
    }

    @Override
    public void jouerCarte(Joueur j) {

        j.ajouteDegat(5);
    }
}
