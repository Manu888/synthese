package com.example.manuel.starrealms.Cards.Vaisseaux.Blob;

import com.example.manuel.starrealms.Cards.Modele.Base;
import com.example.manuel.starrealms.Cards.Modele.Carte;
import com.example.manuel.starrealms.Cards.Modele.Vaisseau;
import com.example.manuel.starrealms.Cards.Modele.CarteAlliee;
import com.example.manuel.starrealms.Entity.Faction;
import com.example.manuel.starrealms.Entity.IA;
import com.example.manuel.starrealms.Entity.Joueur;
import com.example.manuel.starrealms.R;



public class BlobDestroyer extends Vaisseau implements CarteAlliee {

    public BlobDestroyer(){

        this.nom = "BlobDestroyer";
        this.faction = Faction.BLOB;
        this.idImage = R.drawable.blobdestroyer;
        this.cout = 4;
        this.abiliteeUtilise = false;
        this.abiliteeAllieUtilise = false;
    }

    @Override
    public void alliee(Joueur j) {
        if(j instanceof IA){
            ((IA)j).choisirBaseADetruire();
            ((IA)j).feraillerCarteMarche(1);
        }
        else{
            j.faireChoixDetruireBase();
            j.faireChoixFerailleMarche();
        }
    }

    @Override
    public void jouerCarte(Joueur j) {
            j.ajouteDegat(6);
    }
}
