package com.example.manuel.starrealms.Entity;


// Classe utilitaire permettant de tenir les informations récupèrées des champs de la Table halloffame
public class EnregistrementBD {
    String nom;
    int tour;
    int vie;

    public EnregistrementBD(String n, int t, int v){

        this.nom = n;
        this.tour = t;
        this.vie = v;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getTour() {
        return tour;
    }

    public void setTour(int tour) {
        this.tour = tour;
    }

    public int getVie() {
        return vie;
    }

    public void setVie(int vie) {
        this.vie = vie;
    }
}
