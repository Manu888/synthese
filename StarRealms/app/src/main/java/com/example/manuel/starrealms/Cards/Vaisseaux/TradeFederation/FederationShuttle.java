package com.example.manuel.starrealms.Cards.Vaisseaux.TradeFederation;

import com.example.manuel.starrealms.Cards.Modele.Vaisseau;
import com.example.manuel.starrealms.Cards.Modele.CarteAlliee;
import com.example.manuel.starrealms.Entity.Faction;
import com.example.manuel.starrealms.Entity.Joueur;
import com.example.manuel.starrealms.R;


public class FederationShuttle extends Vaisseau implements CarteAlliee {

    public FederationShuttle(){

        this.nom = "FederationShuttle";
        this.faction = Faction.TRADE_FEDERATION;
        this.idImage = R.drawable.federationshuttle;
        this.cout = 1;
        this.abiliteeUtilise = false;
        this.abiliteeAllieUtilise = false;
    }

    @Override
    public void alliee(Joueur j) {
        j.ajouteVie(4);
    }

    @Override
    public void jouerCarte(Joueur j) {
        j.ajouteOr(2);

    }
}
