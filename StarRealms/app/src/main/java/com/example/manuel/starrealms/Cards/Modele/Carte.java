package com.example.manuel.starrealms.Cards.Modele;


import com.example.manuel.starrealms.Cards.Vaisseaux.Scout;
import com.example.manuel.starrealms.Cards.Vaisseaux.Viper;
import com.example.manuel.starrealms.Entity.Faction;
import com.example.manuel.starrealms.Entity.Joueur;

import java.io.Serializable;

// Les cartes implémentents Serializable afin de pouvoir être transmit d'un activité à une autre
public abstract class Carte implements Serializable{
    protected String nom;
    protected Faction faction;
    protected int idImage;
    protected int cout;
    protected boolean abiliteeUtilise;
    protected boolean abiliteeAllieUtilise;


    // GETTERS ET SETTERS

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Faction getFaction() {
        return faction;
    }

    public void setFaction(Faction faction) {
        this.faction = faction;
    }

    public int getIdImage() {
        return idImage;
    }

    public void setIdImage(int idImage) {
        this.idImage = idImage;
    }

    public int getCout() {
        return cout;
    }

    public void setCout(int cout) {
        this.cout = cout;
    }

    public boolean isAbiliteeUtilise() {
        return abiliteeUtilise;
    }

    public void setAbiliteeUtilise(boolean abiliteeUtilise) {
        this.abiliteeUtilise = abiliteeUtilise;
    }

    public boolean isAbiliteeAllieUtilise() {
        return abiliteeAllieUtilise;
    }

    public void setAbiliteeAllieUtilise(boolean abiliteeAllieUtilise) {
        this.abiliteeAllieUtilise = abiliteeAllieUtilise;
    }

    // Méthode de la classe Carte

    // MÉTHODE ABSTRAITE CENTRALE QUI EST IMPLÉMENTÉE DIFFÉREMENT POUR CHAQUE CARTE
    public abstract void jouerCarte(Joueur j);


    // MÉTHODE PERMETTANT D'OBTENIR DES INFORMATIONS SUR LE TYPE DE CARTE
    public boolean isVaisseau() {
        return this instanceof Vaisseau;
    }
    public boolean isBase() {
        return this instanceof Base;
    }
    public boolean isFeraille() {
        return this instanceof CarteFeraille;
    }
    public boolean isAvantPoste() {
        return this instanceof AvantPoste;
    }
    public boolean isBlob() {
        return this.faction == Faction.BLOB;
    }
    public boolean isMachineCult() {
        return this.faction == Faction.MACHINE_CULT;
    }
    public boolean isTradeFederation() {
        return this.faction == Faction.TRADE_FEDERATION;
    }
    public boolean isStarEmpire() {
        return this.faction == Faction.STAR_EMPIRE;
    }
    public boolean isCarteBase() {
        return this instanceof Scout || this instanceof Viper;
    }
    public boolean isAlliee(Carte c) {
        return c.getFaction() == this.getFaction();
    }

    // MÉTHODE PERMETTANT DE SAVOIR COMBIEN DE DÉGAT LA CARTE OCTROIE LORSQU'ELLE EST FÉRAILLER
    public int ajouteCombatFeraille() {return 0;}

    // MÉTHODE PERMETTANT DE SAVOIR COMBIEN D'OR LA CARTE OCTROIE LORSQU'ELLE EST FÉRAILLER
    public int ajouteOrFeraille() {
        return 0;
    }

    //MÉTHODE PERMETTANT DE SAVOIR SI LA CARTE DÉTRUIT UNE BASE LORSQUE FÉRAILLER
    public boolean detruireBaseFeraille() {
        return false;
    }

    // MÉTHODE PERMETTANT DE SAVOIR QUEL CHOIX D'ABILITÉ DE LA CARTE LE JOUEUR A FAIT
    public void choixFait(int choix, Joueur j) {}
}
