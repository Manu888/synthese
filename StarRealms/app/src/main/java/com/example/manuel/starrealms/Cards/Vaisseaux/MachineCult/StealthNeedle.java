package com.example.manuel.starrealms.Cards.Vaisseaux.MachineCult;

import com.example.manuel.starrealms.Cards.Modele.Carte;
import com.example.manuel.starrealms.Cards.Modele.Vaisseau;
import com.example.manuel.starrealms.Entity.Faction;
import com.example.manuel.starrealms.Entity.IA;
import com.example.manuel.starrealms.Entity.Joueur;
import com.example.manuel.starrealms.R;


public class StealthNeedle extends Vaisseau {

    public StealthNeedle(){

        this.nom = "StealthNeedle";
        this.faction = Faction.MACHINE_CULT;
        this.idImage = R.drawable.stealthneedle;
        this.cout = 4;
        this.abiliteeUtilise = false;
        this.abiliteeAllieUtilise = false;
    }

    private Carte carteCopiee;

    public Carte getCarteCopiee() {
        return carteCopiee;
    }
    public void setCarteCopiee(Carte c) {
        carteCopiee = c;
    }

    @Override
    public void jouerCarte(Joueur j) {
        if(j instanceof IA){
            ((IA)j).copieVaisseau(this);
        }
        else{
                j.faireChoixCopieVaisseau(this);
        }
    }

}
