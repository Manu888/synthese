package com.example.manuel.starrealms.Cards.Vaisseaux.TradeFederation;

import com.example.manuel.starrealms.Cards.Modele.Vaisseau;
import com.example.manuel.starrealms.Cards.Modele.CarteAlliee;
import com.example.manuel.starrealms.Entity.Faction;
import com.example.manuel.starrealms.Entity.Joueur;
import com.example.manuel.starrealms.R;


public class TradeEscort extends Vaisseau implements CarteAlliee {

    public TradeEscort(){

        this.nom = "TradeEscort";
        this.faction = Faction.TRADE_FEDERATION;
        this.idImage = R.drawable.tradeescort;
        this.cout = 5;
        this.abiliteeUtilise = false;
        this.abiliteeAllieUtilise = false;
    }

    @Override
    public void alliee(Joueur j) {
        j.pigerCarte(1);
    }

    @Override
    public void jouerCarte(Joueur j) {
        j.ajouteDegat(4);
        j.ajouteVie(4);
    }
}
