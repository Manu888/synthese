package com.example.manuel.starrealms.Cards.Vaisseaux.Blob;

import com.example.manuel.starrealms.Cards.Modele.Carte;
import com.example.manuel.starrealms.Cards.Modele.Vaisseau;
import com.example.manuel.starrealms.Cards.Modele.CarteAlliee;
import com.example.manuel.starrealms.Entity.Faction;
import com.example.manuel.starrealms.Entity.IA;
import com.example.manuel.starrealms.Entity.Joueur;
import com.example.manuel.starrealms.R;

import java.util.Observable;

public class BattlePod extends Vaisseau implements CarteAlliee {

    public BattlePod(){

        this.nom = "BattlePod";
        this.faction = Faction.BLOB;
        this.idImage = R.drawable.battlepod;
        this.cout = 2;
        this.abiliteeUtilise = false;
        this.abiliteeAllieUtilise = false;
    }

    @Override
    public void alliee(Joueur j) {
        j.ajouteDegat(2);
    }

    @Override
    public void jouerCarte(Joueur j) {
        j.ajouteDegat(4);
        if(j instanceof IA){
            ((IA)j).feraillerCarteMarche(1);
        }
        else{
            j.faireChoixFerailleMarche();
        }
    }

    @Override
    public int ajouteCombatFeraille(){
        return 4;
    }

}
