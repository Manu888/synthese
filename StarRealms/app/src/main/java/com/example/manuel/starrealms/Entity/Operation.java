package com.example.manuel.starrealms.Entity;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.Vector;


// Classe permettant de manipuler la BD
public class Operation {
    // Permet de faciliter les opérations sur la base de donnée
    private DatabaseHelper helper;
    private SQLiteDatabase db;

    public Operation(Context c) {
        this.helper = new DatabaseHelper(c);
    }

    // Fonctions permettant d'ouvrir et de fermer l'accès à la base de donnée rapidement
    public void ouvrir(){
        this.db = helper.getWritableDatabase();
    }
    public void fermer(){
        this.db.close();
    }

    // Fonction qui ajoute un joueur sur la Table halloffame
    public void ajouterJoueurHallOfFame(String nom, int tour, int vie){
        // Fonction permettant de d'ajouter un joueur à la table HallofFame
        ContentValues temp = new ContentValues();
        temp.put("nom",nom);
        temp.put("tour",tour);
        temp.put("vie",vie);
        this.db.insert("halloffame",null,temp);
    }

    // Fonction permettant de retourner les 10 meilleurs joueurs de la BD
    public Vector<EnregistrementBD> toutJoueurs(){
        // Fonction retournant toutes les informations de la table de HallofFame sous forme de vecteur
        Cursor c = this.db.rawQuery("SELECT * FROM halloffame ORDER BY TOUR ASC , VIE DESC LIMIT 10", null);
        Vector<EnregistrementBD> vect = new Vector<>();
        while (c.moveToNext()) {
            EnregistrementBD enr = new EnregistrementBD(c.getString(1),c.getInt(2),c.getInt(3));
            vect.add(enr);
        }
        return vect;
    }

}
