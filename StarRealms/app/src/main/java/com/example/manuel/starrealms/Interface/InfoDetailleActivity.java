package com.example.manuel.starrealms.Interface;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import com.example.manuel.starrealms.R;

public class InfoDetailleActivity extends AppCompatActivity {

    ImageView carteDetail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_detaille);

        // On récupère l'image de la carte
        Intent i = getIntent();
        int id = i.getIntExtra("idImage",android.R.color.transparent);
        boolean rotate = i.getBooleanExtra("rotation",false);

        carteDetail = (ImageView)findViewById(R.id.carteDetail);

        carteDetail.setImageResource(id);
        // On tourne la carte de 90 degré si la valeur est vrai ( si c'est une base)
        if(rotate)
            carteDetail.setRotation(-90);

    }
}
