package com.example.manuel.starrealms.Cards.Bases.StarEmpire;

import com.example.manuel.starrealms.Cards.Modele.AvantPoste;
import com.example.manuel.starrealms.Cards.Modele.CarteAlliee;
import com.example.manuel.starrealms.Cards.Modele.CarteFeraille;
import com.example.manuel.starrealms.Entity.Faction;
import com.example.manuel.starrealms.Entity.Joueur;
import com.example.manuel.starrealms.R;


public class SpaceStation extends AvantPoste implements CarteFeraille, CarteAlliee {

    public SpaceStation(){

        this.nom = "SpaceStation";
        this.faction = Faction.STAR_EMPIRE;
        this.idImage = R.drawable.spacestation;
        this.cout = 4;
        this.vie = 4 ;
        this.abiliteeUtilise = false;
        this.abiliteeAllieUtilise = false;
    }

    @Override
    public void alliee(Joueur j) {
        j.ajouteDegat(2);
    }

    @Override
    public void ferailler(Joueur j) {
            j.ajouteOr(4);
    }

    @Override
    public void jouerCarte(Joueur j) {
            j.ajouteDegat(2);
    }

    @Override
    public int ajouteOrFeraille(){
        return 4;
    }
}
