package com.example.manuel.starrealms.Cards.Bases.TradeFederation;

import com.example.manuel.starrealms.Cards.Modele.AvantPoste;
import com.example.manuel.starrealms.Cards.Modele.CarteAlliee;
import com.example.manuel.starrealms.Entity.Choix;
import com.example.manuel.starrealms.Entity.Faction;
import com.example.manuel.starrealms.Entity.IA;
import com.example.manuel.starrealms.Entity.Joueur;
import com.example.manuel.starrealms.R;


public class DefenseCenter extends AvantPoste implements CarteAlliee {

    public DefenseCenter(){

        this.nom = "DefenseCenter";
        this.faction = Faction.TRADE_FEDERATION;
        this.idImage = R.drawable.defensecenter;
        this.cout = 5;
        this.vie=5;
        this.abiliteeUtilise = false;
        this.abiliteeAllieUtilise = false;
    }

    @Override
    public void alliee(Joueur j) {
            j.ajouteDegat(2);
    }

    @Override
    public void jouerCarte(Joueur j) {
        if(j instanceof IA){
            Choix choix1 = new Choix(1,"Ajouter 3 vies");
            Choix choix2 = new Choix(2,"Ajouter 2 dégats");

            ((IA)j).faireChoix(this,choix1,choix2);

        }
        else{
            j.faireChoixVieDegat();
        }
    }
    @Override
    public void choixFait(int choix, Joueur j){

        if(choix == 1){
            j.ajouteVie(3);
        }
        else{
            j.ajouteDegat(2);
        }
    }
}
