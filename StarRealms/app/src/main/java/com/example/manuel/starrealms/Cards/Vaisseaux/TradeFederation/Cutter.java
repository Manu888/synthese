package com.example.manuel.starrealms.Cards.Vaisseaux.TradeFederation;

import com.example.manuel.starrealms.Cards.Modele.Vaisseau;
import com.example.manuel.starrealms.Cards.Modele.CarteAlliee;
import com.example.manuel.starrealms.Entity.Faction;
import com.example.manuel.starrealms.Entity.Joueur;
import com.example.manuel.starrealms.R;


public class Cutter extends Vaisseau implements CarteAlliee {

    public Cutter(){

        this.nom = "Cutter";
        this.faction = Faction.TRADE_FEDERATION;
        this.idImage = R.drawable.cutter;
        this.cout = 2;
        this.abiliteeUtilise = false;
        this.abiliteeAllieUtilise = false;
    }

    @Override
    public void alliee(Joueur j) {
        j.ajouteDegat(4);
    }

    @Override
    public void jouerCarte(Joueur j) {
        j.ajouteVie(4);
        j.ajouteOr(2);
    }
}
