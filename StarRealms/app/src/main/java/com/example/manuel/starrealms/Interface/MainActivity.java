package com.example.manuel.starrealms.Interface;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.manuel.starrealms.R;

public class MainActivity extends AppCompatActivity {

    Button boutonJouer,buttonApprendre, boutonhalloffame;
    Spinner spin;
    String diffChoisie, nom;
    EditText nomJoueur;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // on récupère les éléments visuels de l'activité
        boutonJouer = (Button)findViewById(R.id.jouer);
        buttonApprendre = (Button)findViewById(R.id.apprendre);
        boutonhalloffame = (Button)findViewById(R.id.halloffame);
        nomJoueur = (EditText)findViewById(R.id.editText);

        // Remplir le spinner des choix de difficulté possible et on associe l'adapteur
        spin = (Spinner)findViewById(R.id.spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.difficultes, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spin.setAdapter(adapter);
        spin.setSelection(1);

        // On met des écouteurs sur chacun des boutons
        boutonJouer.setOnClickListener(new Ecouteur());
        buttonApprendre.setOnClickListener(new Ecouteur());
        boutonhalloffame.setOnClickListener(new Ecouteur());


    }

    public class Ecouteur implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            if(v == boutonJouer){
                // On démarre l'activité de jeu après lui avoir passer le nom du joueur, la difficulté de l'IA
                Intent myIntent = new Intent(MainActivity.this, GameActivity.class);
                diffChoisie= spin.getSelectedItem().toString();
                nom = nomJoueur.getText().toString();
                myIntent.putExtra("nom",nom);
                myIntent.putExtra("difficulte",diffChoisie);
                startActivity(myIntent);
            }
            else if(v == buttonApprendre){
                // On démarre un fureteur internet avec le lien d'apprentissge de Star Realms
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.starrealms.com/learn-to-play/"));
                startActivity(browserIntent);
            }
            else if(v == boutonhalloffame){
                // On démarre l'activité du Hall of Fame
                Intent i = new Intent(MainActivity.this, HallOfFameActivity.class);
                startActivity(i);
            }
        }
    }
}
