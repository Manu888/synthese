package com.example.manuel.starrealms.Cards.Vaisseaux.Blob;

import com.example.manuel.starrealms.Cards.Modele.Carte;
import com.example.manuel.starrealms.Cards.Modele.Vaisseau;
import com.example.manuel.starrealms.Cards.Modele.CarteAlliee;
import com.example.manuel.starrealms.Entity.Faction;
import com.example.manuel.starrealms.Entity.IA;
import com.example.manuel.starrealms.Entity.Joueur;
import com.example.manuel.starrealms.R;


public class BlobCarrier extends Vaisseau implements CarteAlliee {

    public BlobCarrier(){

        this.nom = "BlobCarrier";
        this.faction = Faction.BLOB;
        this.idImage = R.drawable.blobcarrier;
        this.cout = 6;
        this.abiliteeUtilise = false;
        this.abiliteeAllieUtilise = false;
    }

    @Override
    public void alliee(Joueur j) {
        if(j instanceof IA){
            ((IA)j).choisirAchatGratuit();
        }
        else {
            j.faireChoixAchatGratuit();
        }

    }

    @Override
    public void jouerCarte(Joueur j) {
        j.ajouteDegat(7);
    }
}
