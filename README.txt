README r�alis� le 7 d�cembre 2017 par Manuel Piquette

Le pr�sent projet se d�roule sous Android dans un langage Java, c'est pourquoi il n�c�ssite Android Studio pour simuler un t�l�phone Android. 
Le projet Star Realms a �t� r�alis� sous Android Studio 3.01 avec un Gradle 3.0. L'installation se fait simplement via l'�xecutable sur le
site suivant : https://developer.android.com/studio/index.html Il est possible de faire toutes les mise a jours possibles et ensuite lancer
le projet afin de v�rifier la compatibilit�. Dans le pire des cas, l'IDE d'Android Studio nous proposera de modifier le gradle afin de le
rendre compatible avec la version du Studio. Il est pr�f�rable d'avoir la plus r�cente version du SDK et de la machine de compilation Java afin
favoriser la compatibilit�. (https://www.java.com/fr/download/). Il est possible qu'il soit necc�saire d'installer le plugin de JetBrain Statistics
qui se retrouve au lien suivant : https://plugins.jetbrains.com/plugin/4509-statistic. Afin de l'associer au projet, vous allez devoir aller dans
File -> Settings -> Plugins -> Install plugin from disk.

Suite � l'installation, vous allez devoir cr�er un �mulateur afin de pouvoir lancer le projet sur un t�l�phone. (https://developer.android.com/studio/run/managing-avds.html)
Pour ce faire, il suffit de cliquer sur Tools -> AVD Manager puis Create a New Virtual Device. Vous allez devoir choisir un API de niveau 24 et +
afin de pouvoir profiter de toutes les fonctionalit�s du projet. Il est aussi possible que vous ayez besoin d'activer la gestion des fonctions
lambdas via l'interface de l'IDE. Dans le cas �cheant, un message d'avertissement vous proposera d'activer la fonctionalit�. Restez � l'aff�t !

Il ne suffit plus qu'� ouvrir le projet � l'aide de File -> Open puis de laisser le Gradle Build se construire ( v�rifier le bas de l'interface)
Vous allez ensuite pouvoir lancer l'�mulateur par la commande MAJ + F10. L'�mulateur prend quelques minutes afin de s'initialiser et se lancer 
compl�tement. Vous allez ensuite pouvoir cliquer sur l'interface afin d'int�ragir avec celui-ci, un peu comme un doigt le ferait avec un �cran
tactile.